﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Register Src="~/views/control/ucLoadControl.ascx" TagPrefix="uc1" TagName="ucLoadControl" %>
<%@ Register Src="~/views/control/ucBanner.ascx" TagPrefix="uc2" TagName="ucBanner" %>
<%@ Register Src="~/views/control/ucSearchBox.ascx" TagPrefix="uc3" TagName="ucSearchBox" %>
<%@ Register Src="~/views/control/ucMenuTop.ascx" TagPrefix="uc4" TagName="ucMenuTop" %>
<%@ Register Src="~/views/control/ucLanguage.ascx" TagPrefix="uc5" TagName="ucLanguage" %>
<%@ Register Src="~/views/control/ucLinkAcount.ascx" TagPrefix="uc6" TagName="ucLinkAcount" %>
<%@ Register Src="~/views/control/ucShopCart.ascx" TagPrefix="uc7" TagName="ucShopCart" %>
<%@ Register Src="~/views/control/ucMenuMain.ascx" TagPrefix="uc8" TagName="ucMenuMain" %>
<%@ Register Src="~/views/control/ucSlides.ascx" TagPrefix="uc9" TagName="ucSlides" %>
<%@ Register Src="~/views/control/ucLogin.ascx" TagPrefix="uc10" TagName="ucLogin" %>
<%@ Register Src="~/views/control/ucListCategory.ascx" TagPrefix="uc11" TagName="ucListCategory" %>
<%@ Register Src="~/views/control/ucOnline.ascx" TagPrefix="uc12" TagName="ucOnline" %>
<%@ Register Src="~/views/news/ucNewsHot.ascx" TagPrefix="uc13" TagName="ucNewsHot" %>
<%@ Register Src="~/views/products/ucProSale.ascx" TagPrefix="uc14" TagName="ucProSale" %>
<%@ Register Src="~/views/control/ucAdvleft.ascx" TagPrefix="uc15" TagName="ucAdvleft" %>
<%@ Register Src="~/views/control/ucBoxface.ascx" TagPrefix="uc16" TagName="ucBoxface" %>
<%@ Register Src="~/views/control/ucOnlineVisit.ascx" TagPrefix="uc17" TagName="ucOnlineVisit" %>
<%@ Register Src="~/views/control/ucAdvright.ascx" TagPrefix="uc18" TagName="ucAdvright" %>
<%@ Register Src="~/views/products/ucProNew.ascx" TagPrefix="uc19" TagName="ucProNew" %>
<%@ Register Src="~/views/news/ucAboutUs.ascx" TagPrefix="uc20" TagName="ucAboutUs" %>
<%@ Register Src="~/views/products/ucProHome.ascx" TagPrefix="uc21" TagName="ucProHome" %>
<%@ Register Src="~/views/products/ucProHomeParent.ascx" TagPrefix="uc22" TagName="ucProHomeParent" %>
<%@ Register Src="~/views/news/ucNewsHomePriority.ascx" TagPrefix="uc23" TagName="ucNewsHomePriority" %>
<%@ Register Src="~/views/news/ucNewsHome.ascx" TagPrefix="uc24" TagName="ucNewsHome" %>
<%@ Register Src="~/views/control/ucMenuFooter.ascx" TagPrefix="uc25" TagName="ucMenuFooter" %>
<%@ Register Src="~/views/control/ucSocialNetwork.ascx" TagPrefix="uc26" TagName="ucSocialNetwork" %>
<%@ Register Src="~/views/control/ucFooter.ascx" TagPrefix="uc27" TagName="ucFooter" %>
<%@ Register Src="~/views/control/ucMenuLeft.ascx" TagPrefix="uc28" TagName="ucMenuLeft" %>
<%@ Register Src="~/views/products/ucFilterAttributes.ascx" TagPrefix="uc29" TagName="ucFilterAttributes" %>
<%@ Register Src="~/views/control/ucAdvCenter.ascx" TagPrefix="uc30" TagName="ucAdvCenter" %>
<%@ Register Src="~/views/library/ucListVideo.ascx" TagPrefix="uc31" TagName="ucListVideo" %>
<%@ Register Src="~/views/products/ucProHot.ascx" TagPrefix="uc32" TagName="ucProHot" %>
<%@ Register Src="~/views/products/ucProMotion.ascx" TagPrefix="uc33" TagName="ucProMotion" %>
<%@ Register Src="~/views/control/ucBreadcrumb.ascx" TagPrefix="uc34" TagName="ucBreadcrumb" %>
<%@ Register Src="~/views/control/ucLinkwebsite.ascx" TagPrefix="uc35" TagName="ucLinkwebsite" %>
<%@ Register Src="~/views/control/ucNewsLetter.ascx" TagPrefix="uc36" TagName="ucNewsLetter" %>
<%@ Register Src="~/views/products/ucViewedProducts.ascx" TagPrefix="uc37" TagName="ucViewedProducts" %>
<%@ Register Src="~/views/news/ucPriority.ascx" TagPrefix="uc38" TagName="ucPriority" %>
<%@ Register Src="~/views/tour/ucTourHome.ascx" TagPrefix="uc39" TagName="ucTourHome" %>
<%@ Register Src="~/views/Tour/ucTourNew.ascx" TagPrefix="uc42" TagName="ucTourNew" %>
<%@ Register Src="~/views/Tour/ucTourSale.ascx" TagPrefix="uc43" TagName="ucTourSale" %>
<%@ Register Src="~/views/Tour/ucTourTop.ascx" TagPrefix="uc44" TagName="ucTourTop" %>
<%@ Register Src="~/views/Tour/listTour.ascx" TagPrefix="uc45" TagName="listTour" %>
<%@ Register Src="~/views/Tour/ucTourByTopicHome.ascx" TagPrefix="uc46" TagName="ucTourByTopicHome" %>
<%@ Register Src="~/views/search/ucProSearch.ascx" TagPrefix="uc47" TagName="ucProSearch" %>
<%@ Register Src="~/views/search/ucTourSearch.ascx" TagPrefix="uc48" TagName="ucTourSearch" %>
<%@ Register Src="~/views/Tour/ucCatTourHome.ascx" TagPrefix="uc50" TagName="ucCatTourHome" %>
<%@ Register Src="~/views/control/ucBanner.ascx" TagPrefix="uc51" TagName="ucBanner2" %>
<%@ Register Src="~/views/products/ucProductByTab.ascx" TagPrefix="uc52" TagName="ucProductByTab" %>
<%@ Register Src="~/views/products/ucProSaleV2.ascx" TagPrefix="uc53" TagName="ucProSaleV2" %>

<%@ Register Src="~/views/news/ucNewsEvent.ascx" TagPrefix="uc54" TagName="ucNewsEvent" %>
<%@ Register Src="~/views/pages/ucBookNow.ascx" TagPrefix="uc100" TagName="ucBookNow" %>
<%@ Register Src="~/views/tour/ucTourHot.ascx" TagPrefix="uc1001" TagName="ucTourHot" %>
<%@ Register Src="~/views/tour/ucTourSearchBox.ascx" TagPrefix="uc1002" TagName="ucTourSearchBox" %>
<%@ Register Src="~/views/control/ucCustomerReply.ascx" TagPrefix="uc1004" TagName="ucCustomerReply" %>
<%@ Register Src="~/views/project/ucProjectHot.ascx" TagPrefix="uc1005" TagName="ucProjectHot" %>
<%@ Register Src="~/views/project/ucProjectSale.ascx" TagPrefix="uc1006" TagName="ucProjectSale" %>
<%@ Register Src="~/views/project/ucProjectRegister.ascx" TagPrefix="uc1007" TagName="ucProjectRegister" %>
<%@ Register Src="~/views/products/ucProContact.ascx" TagPrefix="uc1008" TagName="ucProContact" %>

<div class="btn-close"></div>
<div class="close_dk"></div>
<div class="web-site">
    <!--==== header ====-->
    <div class="container-fluid header">
        <div class="container">
            <div class="col-md-3 logo">
                <uc2:ucBanner runat="server" ID="ucBanner" />
            </div>
            <div class="col-md-9 header-info">
                <ul class="head-info">
                    <li class="col-md-5 address">
                        <span class="header-address">
                            <i class="fa fa-map-marker"></i>
                        </span>
                        <div class="text-address">Địa chỉ</div>
                        <div class="text-address2">Phòng 910, Tầng 09, Sảnh A2, Tòa Nhà Ecolife Capital, Lê Văn Lương, Hà Nội</div>
                    </li>
                    <li class="col-md-4 phone">
                        <span class="header-phone">
                            <i class="fa fa-phone"></i>
                        </span>
                        <div class="text-phone">Hotline</div>
                        <div class="text-phone2">0934.493.222 - 0904.995.636</div>
                    </li>
                    <li class="col-md-3 mxh">
                        <uc26:ucSocialNetwork runat="server" ID="ucSocialNetworkTop" />
                    </li>
                </ul>
            </div>
        </div>
        <!-- ==== Navication ==== -->
        <div class="container navication">
            <div class="nav">
                <div class="col-md-9 menu-chinh">
                    <uc8:ucMenuMain runat="server" ID="ucMenuMain" />
                </div>
                <div class="col-md-3 tim-kiem">
                    <uc3:ucSearchBox runat="server" ID="ucSearchBox" />
                </div>
                <div class="col-md-12 mobile_click"><div class="menu_mobile"><i class="fa fa-bars" aria-hidden="true"></i>Menu</div></div>
            </div>
        </div>
    </div>    

   
    
    <%if (Session["home_page"] != null)
      {%>
    <!--==== slider ====-->
    <div class="container-fluid slider">
        <uc9:ucSlides runat="server" ID="ucSlides" />
    </div>
    <div class="container-fluid content">
        <uc1008:ucProContact runat="server" ID="ucProContact" />
    </div>
    <div class="container-fluid content">
        <div class="container project-in-home">            
            <uc1006:ucProjectSale runat="server" ID="ucProjectSale" />
        </div>
    </div> 

    <div class="container-fluid project_dev">
        <div class="container">
            <uc1005:ucProjectHot runat="server" ID="ucProjectHot" />
        </div>
    </div>   

    <div class="container-fluid dang-ky-tu-van">
        <div class="container">
            <uc1007:ucProjectRegister runat="server" ID="ucProjectRegister" />
            <div class="line5">
                <img src="uploads/layout/default/css/images/line5.png" alt="" />
            </div>
        </div>
    </div>    

    <div class="container-fluid content2">
        <div class="container">
            <div class="row">
                <div class="col-md-4 about_us">
                    <uc20:ucAboutUs runat="server" ID="ucAboutUs" />
                    <div class="xt"><a href="http://sankinhdo.vn/gioi-thieu.html">Xem thêm</a></div>
                </div>
                <div class="col-md-4 news_home">
                    <uc24:ucNewsHome runat="server" ID="ucNewsHome" />
                    <div class="xt"><a href="http://sankinhdo.vn/tin-tuc-su-kien.html">Xem thêm</a></div>
                </div>
                <div class="col-md-4 list_video">
                    <uc31:ucListVideo runat="server" ID="ucListVideo" />
                </div>
                <div class="line2">
                    <img src="uploads/layout/default/css/images/line2.jpg" alt="" /></div>
                <div class="line3">
                    <img src="uploads/layout/default/css/images/line3.jpg" alt="" /></div>
                <div class="line4">
                    <img src="uploads/layout/default/css/images/line4.jpg" alt="" /></div>
            </div>
        </div>
    </div>

    
    <%} %>

    <%if (Session["home_page"] == null)
      {%>
    <div class="container main-page-else">
        <div class="row">
            <div class="col-md-12"> 
                <uc34:ucBreadcrumb runat="server" ID="ucBreadcrumb" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col_left">
                <uc54:ucNewsEvent runat="server" ID="ucNewsEvent" />
                <div class="clearfix"></div>
                <uc28:ucMenuLeft runat="server" ID="ucMenuLeft" />
                <div class="clearfix"></div>
                <uc1007:ucProjectRegister runat="server" ID="ucProjectRegister1" />
                <div class="clearfix"></div>
                <uc15:ucAdvleft runat="server" ID="ucAdvleft" />
                <div class="clearfix"></div>
                <uc38:ucPriority runat="server" ID="ucPriority" />
            </div>
            <div class="col-md-9 col_right">
                <uc1:ucLoadControl runat="server" ID="ucLoadControl" />
            </div>
        </div>
    </div>
    <%} %>
    <div class="container-fluid tu-van">
        <div class="container">
            <uc1004:ucCustomerReply runat="server" ID="ucCustomerReply" />
            <div class="line">
                <img src="uploads/layout/default/css/images/line.jpg" alt="" /></div>
        </div>
    </div>
    
    <div class="container-fluid footer">
        <div class="container">
            <div class="col-md-4">
                <uc51:ucBanner2 runat="server" ID="ucBanner2" />
                <uc27:ucFooter runat="server" ID="ucFooter" />
                <uc26:ucSocialNetwork runat="server" ID="ucSocialNetworkBottom" />
            </div>
            <div class="col-md-4">
                <uc25:ucMenuFooter runat="server" ID="ucMenuFooter" />
            </div>
            <div class="col-md-4">
                <uc16:ucBoxface runat="server" ID="ucBoxface" />
            </div>
        </div>
    </div>
    <div class="container-fluid coppy-right">Bản quyền thuộc về công ty Bất Động Sản Kinh Đô</div>
</div>

<a class="call_now" href="tel:0934.493.222"><i class="fa fa-phone" aria-hidden="true"></i> Gọi ngay</a>
<div class="click_dk_all"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Đăng ký tư vấn</div>

<div class="dk_all">
    <div class="header"><i class="fa fa-times" aria-hidden="true"></i> ĐĂNG KÝ TƯ VẤN</div>
    <!-- <uc100:ucBookNow runat="server" ID="ucBookNow" /> -->
    <uc1007:ucProjectRegister runat="server" ID="ucProjectRegister2" />
</div>
<div class="to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></div>

<uc18:ucAdvright runat="server" ID="ucAdvright" />

<script type="text/javascript">
    $(document).ready(function () {
        $("#content div").hide(); // Initially hide all content
        $("#tabs li:first").attr("id", "current"); // Activate first tab
        $("#content div:first").fadeIn(); // Show first tab content

        $('#tabs a').click(function (e) {
            e.preventDefault();
            if ($(this).closest("li").attr("id") == "current") { //detection for current tab
                return
            }
            else {
                $("#content div").hide(); //Hide all content
                $("#tabs li").attr("id", ""); //Reset id's
                $(this).parent().attr("id", "current"); // Activate this
                $('#' + $(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });
    });



    var owl_category_1 = $(".box-adv-center .body-adv-center");
    owl_category_1.owlCarousel({
        items: 1,
        loop: true,
        autoPlay: true,
        transitionStyle: "fade",
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [991, 1],
        itemsTablet: [769, 1],
        itemsTablet: [641, 1],
        itemsTablet: [640, 1],
        itemsMobile: [320, 1],
        navigation: false,
        navigationText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        rewindNav: true,
        scrollPerPage: true,
        slideSpeed: 1500,
        pagination: true,
        paginationNumbers: false,
    });


    var owl_category_2 = $(".col-md-8 .custumer-say");
    owl_category_2.owlCarousel({
        items: 2,
        loop: true,
        autoPlay: true,
        transitionStyle: "fade",
        itemsDesktop: [1199, 2],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [769, 2],
        itemsTablet: [641, 1],
        itemsTablet: [640, 1],
        itemsMobile: [320, 1],
        navigation: false,
        navigationText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        rewindNav: true,
        scrollPerPage: true,
        slideSpeed: 1500,
        pagination: true,
        paginationNumbers: false,
    });

    var owl_category3 = $(".custumer-say");
    owl_category3.owlCarousel({
        items: 4,
        autoPlay: true,
        transitionStyle: "fade",
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [640, 2],
        itemsMobile: [320, 1],
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        rewindNav: true,
        scrollPerPage: true,
        slideSpeed: 1500,
        pagination: false,
        paginationNumbers: false,
    });

    $(document).ready(function() {
        $(".slides .owl-prev").html('<i class="fa fa-angle-left" aria-hidden="true"></i>');
        $(".slides .owl-next").html('<i class="fa fa-angle-right" aria-hidden="true"></i>');
    });
</script>
