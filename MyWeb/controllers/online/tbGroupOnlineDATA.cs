﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tbGroupOnlineDATA
{
    #region[Declare variables]
    private string _ID;
    private string _gName;
    private string _gOrd;
    #endregion
    #region[Function]
    public tbGroupOnlineDATA() { }
    public tbGroupOnlineDATA(string ID_, string gName_, string gOrd_)
    {
        _ID = ID_;
        _gName = gName_;
        _gOrd = gOrd_;
    }
    #endregion
    #region[Assigned value]
    public string ID { get { return _ID; } set { _ID = value; } }
    public string gName { get { return _gName; } set { _gName = value; } }
    public string gOrd { get { return _gOrd; } set { _gOrd = value; } }
    #endregion
}