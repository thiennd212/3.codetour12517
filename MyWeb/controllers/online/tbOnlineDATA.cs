﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tbOnlineDATA
{
    #region[Declare variables]
    private string _onId;
    private string _onName;
    private string _onName1;
    private string _onTell;
    private string _onNick;
    private string _onActive;
    private string _onOrd;
    private string _onType;
    private string _onLang;
    private string _onSkype;
    private string _onEmail;
    private string _onYahoo;
    private string _onFace;
    private string _onZalo;
    #endregion
    #region[Function]
    public tbOnlineDATA() { }
    public tbOnlineDATA(string onId_, string onName_, string onName1_, string onTell_, string onNick_, string onActive_, string onOrd_, string onType_, string onLang_, string onSkype_, string onEmail_, string onYahoo_, string onFace_, string onZalo_)
    {
        _onId = onId_;
        _onName = onName_;
        _onName1 = onName1_;
        _onTell = onTell_;
        _onNick = onNick_;
        _onActive = onActive_;
        _onOrd = onOrd_;
        _onType = onType_;
        _onLang = onLang_;
        _onSkype = onSkype_;
        _onEmail = onEmail_;
        _onYahoo = onYahoo_;
        _onFace = onFace_;
        _onZalo = onZalo_;
    }
    #endregion
    #region[Assigned value]
    public string onId { get { return _onId; } set { _onId = value; } }
    public string onName { get { return _onName; } set { _onName = value; } }
    public string onName1 { get { return _onName1; } set { _onName1 = value; } }
    public string onTell { get { return _onTell; } set { _onTell = value; } }
    public string onNick { get { return _onNick; } set { _onNick = value; } }
    public string onActive { get { return _onActive; } set { _onActive = value; } }
    public string onOrd { get { return _onOrd; } set { _onOrd = value; } }
    public string onType { get { return _onType; } set { _onType = value; } }
    public string onLang { get { return _onLang; } set { _onLang = value; } }
    public string onSkype { get { return _onSkype; } set { _onSkype = value; } }
    public string onEmail { get { return _onEmail; } set { _onEmail = value; } }
    public string onYahoo { get { return _onYahoo; } set { _onYahoo = value; } }
    public string onFace { get { return _onFace; } set { _onFace = value; } }
    public string onZalo { get { return _onZalo; } set { _onZalo = value; } }
    #endregion
}