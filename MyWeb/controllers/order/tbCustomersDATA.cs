﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tbCustomersDATA
{
    #region[Declare variables]
    private string _id;
    private string _Acount;
    private string _Pas;
    private string _name;
    private string _mail;
    private string _Address;
    private string _phone;
    private string _detail;
    private string _totalmoney;
    private string _lang;
    private string _Tell;
    private string _Payment;
    private string _proid;
    private string _cus_id;
    private string _proname;
    private string _price;
    private string _number;
    private string _money;
    private string _status;
    private string _createdate;
    private string groupname;

    #endregion
    #region[Function]
    public tbCustomersDATA() { }
    public tbCustomersDATA(string id_, string Acount_, string Pas_, string name_, string mail_, string Address_, string phone_, string detail_, string totalmoney_, string lang_, string Tell_, string Payment_, string status_)
    {
        _id = id_;
        _Acount = Acount_;
        _Pas = Pas_;
        _name = name_;
        _mail = mail_;
        _Address = Address_;
        _phone = phone_;
        _detail = detail_;
        _totalmoney = totalmoney_;
        _lang = lang_;
        _Tell = Tell_;
        _Payment = Payment_;
        _status = status_;
    }
    #endregion
    #region[Assigned value]
    public string Groupname { get { return groupname; } set { groupname = value; } }
    public string id { get { return _id; } set { _id = value; } }
    public string Acount { get { return _Acount; } set { _Acount = value; } }
    public string Pas { get { return _Pas; } set { _Pas = value; } }
    public string name { get { return _name; } set { _name = value; } }
    public string mail { get { return _mail; } set { _mail = value; } }
    public string Address { get { return _Address; } set { _Address = value; } }
    public string phone { get { return _phone; } set { _phone = value; } }
    public string detail { get { return _detail; } set { _detail = value; } }
    public string totalmoney { get { return _totalmoney; } set { _totalmoney = value; } }
    public string lang { get { return _lang; } set { _lang = value; } }
    public string Tell { get { return _Tell; } set { _Tell = value; } }
    public string Payment { get { return _Payment; } set { _Payment = value; } }
    public string proid { get { return _proid; } set { _proid = value; } }
    public string cus_id { get { return _cus_id; } set { _cus_id = value; } }
    public string proname { get { return _proname; } set { _proname = value; } }
    public string price { get { return _price; } set { _price = value; } }
    public string number { get { return _number; } set { _number = value; } }
    public string money { get { return _money; } set { _money = value; } }
    public string status { get { return _status; } set { _status = value; } }
    public string createdate { get { return _createdate; } set { _createdate = value; } }
    #endregion
}