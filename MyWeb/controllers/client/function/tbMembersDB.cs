﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class tbMembersDB
{
    #region[tbMembers_Add]
    public static bool tbMembers_Add(tbMembersDATA _tbMembersDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbMembers_Add", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@uID", _tbMembersDATA.uID));
                dbCmd.Parameters.Add(new SqlParameter("@uPas", _tbMembersDATA.uPas));
                dbCmd.Parameters.Add(new SqlParameter("@Email", _tbMembersDATA.Email));
                dbCmd.Parameters.Add(new SqlParameter("@Add", _tbMembersDATA.Add));
                dbCmd.Parameters.Add(new SqlParameter("@Tell", _tbMembersDATA.Tell));
                dbCmd.Parameters.Add(new SqlParameter("@Fax", _tbMembersDATA.Fax));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbMembers_Update]
    public static bool tbMembers_Update(tbMembersDATA _tbMembersDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbMembers_Update", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@ID", _tbMembersDATA.ID));
                dbCmd.Parameters.Add(new SqlParameter("@uID", _tbMembersDATA.uID));
                dbCmd.Parameters.Add(new SqlParameter("@uPas", _tbMembersDATA.uPas));
                dbCmd.Parameters.Add(new SqlParameter("@Email", _tbMembersDATA.Email));
                dbCmd.Parameters.Add(new SqlParameter("@Add", _tbMembersDATA.Add));
                dbCmd.Parameters.Add(new SqlParameter("@Tell", _tbMembersDATA.Tell));
                dbCmd.Parameters.Add(new SqlParameter("@Fax", _tbMembersDATA.Fax));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbMembers_Update_Counts]
    public static bool tbMembers_Update_Counts(tbMembersDATA _tbMembersDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbMembers_Update_Counts", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@ID", _tbMembersDATA.ID));
                dbCmd.Parameters.Add(new SqlParameter("@Counts", _tbMembersDATA.Counts));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbMembers_Update_Active]
    public static bool tbMembers_Update_Active(tbMembersDATA _tbMembersDATA)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbMembers_Update_Active", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@ID", _tbMembersDATA.ID));
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbMembers_Delete]
    public static bool tbMembers_Delete(string ID)
    {
        bool check = false;
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbMembers_Delete", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbCmd.Parameters.Add(new SqlParameter("@ID", ID));
                dbConn.Open();
                dbCmd.ExecuteNonQuery();
                dbConn.Close();
                check = true;
            }
        }
        return check;
    }
    #endregion
    #region[tbMembers_GetByAll]
    public static List<tbMembersDATA> tbMembers_GetByAll()
    {
        List<tbMembersDATA> list = new List<tbMembersDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbMembers_GetByAll", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbMembersDATA objtbMembersDATA = new tbMembersDATA(
                        reader["ID"].ToString(),
                        reader["uID"].ToString(),
                        reader["uPas"].ToString(),
                        reader["Email"].ToString(),
                        reader["Add"].ToString(),
                        reader["Tell"].ToString(),
                        reader["Fax"].ToString(),
                        reader["Counts"].ToString(),
                        reader["Active"].ToString());
                        list.Add(objtbMembersDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbMembers_GetByID]
    public static List<tbMembersDATA> tbMembers_GetByID(string ID)
    {
        List<tbMembersDATA> list = new List<tbMembersDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbMembers_GetByID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@ID", ID));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbMembersDATA objtbMembersDATA = new tbMembersDATA(
                        reader["ID"].ToString(),
                        reader["uID"].ToString(),
                        reader["uPas"].ToString(),
                        reader["Email"].ToString(),
                        reader["Add"].ToString(),
                        reader["Tell"].ToString(),
                        reader["Fax"].ToString(),
                        reader["Counts"].ToString(),
                        reader["Active"].ToString());
                        list.Add(objtbMembersDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[sp_tbMembers_GetByMaxID]
    public static List<tbMembersDATA> tbMembers_GetByMaxID()
    {
        List<tbMembersDATA> list = new List<tbMembersDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbMembers_GetByMaxID", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbMembersDATA objtbMembersDATA = new tbMembersDATA(
                        reader["ID"].ToString(),
                        reader["uID"].ToString(),
                        reader["uPas"].ToString(),
                        reader["Email"].ToString(),
                        reader["Add"].ToString(),
                        reader["Tell"].ToString(),
                        reader["Fax"].ToString(),
                        reader["Counts"].ToString(),
                        reader["Active"].ToString());
                        list.Add(objtbMembersDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbMembers_GetByUser]
    public static List<tbMembersDATA> tbMembers_GetByUser(string uID)
    {
        List<tbMembersDATA> list = new List<tbMembersDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbMembers_GetByUser", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@uID", uID));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbMembersDATA objtbMembersDATA = new tbMembersDATA(
                        reader["ID"].ToString(),
                        reader["uID"].ToString(),
                        reader["uPas"].ToString(),
                        reader["Email"].ToString(),
                        reader["Add"].ToString(),
                        reader["Tell"].ToString(),
                        reader["Fax"].ToString(),
                        reader["Counts"].ToString(),
                        reader["Active"].ToString());
                        list.Add(objtbMembersDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbMembers_GetByEmail]
    public static List<tbMembersDATA> tbMembers_GetByEmail(string Email)
    {
        List<tbMembersDATA> list = new List<tbMembersDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbMembers_GetByEmail", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@Email", Email));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbMembersDATA objtbMembersDATA = new tbMembersDATA(
                        reader["ID"].ToString(),
                        reader["uID"].ToString(),
                        reader["uPas"].ToString(),
                        reader["Email"].ToString(),
                        reader["Add"].ToString(),
                        reader["Tell"].ToString(),
                        reader["Fax"].ToString(),
                        reader["Counts"].ToString(),
                        reader["Active"].ToString());
                        list.Add(objtbMembersDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbMembers_GetByuIDEmail]
    public static List<tbMembersDATA> tbMembers_GetByuIDEmail(string uID, string Email)
    {
        List<tbMembersDATA> list = new List<tbMembersDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbMembers_GetByuIDEmail", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@uID", uID));
                dbCmd.Parameters.Add(new SqlParameter("@Email", Email));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbMembersDATA objtbMembersDATA = new tbMembersDATA(
                        reader["ID"].ToString(),
                        reader["uID"].ToString(),
                        reader["uPas"].ToString(),
                        reader["Email"].ToString(),
                        reader["Add"].ToString(),
                        reader["Tell"].ToString(),
                        reader["Fax"].ToString(),
                        reader["Counts"].ToString(),
                        reader["Active"].ToString());
                        list.Add(objtbMembersDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
    #region[tbMembers_GetByLogin]
    public static List<tbMembersDATA> tbMembers_GetByLogin(string uID, string uPas)
    {
        List<tbMembersDATA> list = new List<tbMembersDATA>();
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
        {
            using (SqlCommand dbCmd = new SqlCommand("sp_tbMembers_GetByLogin", dbConn))
            {
                dbCmd.CommandType = CommandType.StoredProcedure;
                dbConn.Open();
                dbCmd.Parameters.Add(new SqlParameter("@uID", uID));
                dbCmd.Parameters.Add(new SqlParameter("@uPas", uPas));
                using (SqlDataReader reader = dbCmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tbMembersDATA objtbMembersDATA = new tbMembersDATA(
                        reader["ID"].ToString(),
                        reader["uID"].ToString(),
                        reader["uPas"].ToString(),
                        reader["Email"].ToString(),
                        reader["Add"].ToString(),
                        reader["Tell"].ToString(),
                        reader["Fax"].ToString(),
                        reader["Counts"].ToString(),
                        reader["Active"].ToString());
                        list.Add(objtbMembersDATA);
                    }
                }
                dbConn.Close();
            }
        }
        return list;
    }
    #endregion
}