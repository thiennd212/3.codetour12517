﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class tbProjectDATA
{
    #region[Declare variables]
    private string _proId;
    private string _proCode;
    private string _proName;
    private string _proImage;
    private string _proFiles;
    private string _proContent;
    private string _proDetail;
    private string _proDate;
    private string _proTitle;
    private string _proDescription;
    private string _proKeyword;
    private string _proPriority;
    private string _proIndex;
    private string _catId;
    private string _proType;
    private string _proActive;
    private string _proOriginalPrice;
    private string _proPrice;
    private string _proUnitprice;
    private string _proPromotions;
    private string _proWeight;
    private string _proUnits;
    private string _proOrd;
    private string _proLang;
    private string _proWarranty;
    private string _proTagName;
    private string _proVideo;
    private string _proCount;
    private string _proCare;
    private string _proSplq;
    private string _proStatus;
    private string _manufacturerId;
    #endregion
    #region[Function]

    public tbProjectDATA() { }
    public tbProjectDATA(string proId_, string proCode_, string proName_, string proImage_, 
        string proFiles_, string proContent_, string proDetail_, string proDate_, 
        string proTitle_, string proDescription_, string proKeyword_, string proPriority_, 
        string proIndex_, string catId_, string proType_, string proActive_, 
        string proOriginalPrice_, string proPrice_, string proUnitprice_, string proPromotions_, 
        string proWeight_, string proUnits_, string proOrd_, string proLang_, 
        string proWarranty_, string proTagName_, string proVideo_, string proCount_,
        string proCare_, string proStatus_, string manufacturerId_)
    {
        _proId = proId_;
        _proCode = proCode_;
        _proName = proName_;
        _proImage = proImage_;
        _proFiles = proFiles_;
        _proContent = proContent_;
        _proDetail = proDetail_;
        _proDate = proDate_;
        _proTitle = proTitle_;
        _proDescription = proDescription_;
        _proKeyword = proKeyword_;
        _proPriority = proPriority_;
        _proIndex = proIndex_;
        _catId = catId_;
        _proType = proType_;
        _proActive = proActive_;
        _proOriginalPrice = proOriginalPrice_;
        _proPrice = proPrice_;
        _proUnitprice = proUnitprice_;
        _proPromotions = proPromotions_;
        _proWeight = proWeight_;
        _proUnits = proUnits_;
        _proOrd = proOrd_;
        _proLang = proLang_;
        _proWarranty = proWarranty_;
        _proTagName = proTagName_;
        _proVideo = proVideo_;
        _proCount = proCount_;
        _proCare = proCare_;
        _proStatus = proStatus_;
        _manufacturerId = manufacturerId_;
    }
    #endregion
    #region[Assigned value]
    public string proId { get { return _proId; } set { _proId = value; } }
    public string proCode { get { return _proCode; } set { _proCode = value; } }
    public string proName { get { return _proName; } set { _proName = value; } }
    public string proImage { get { return _proImage; } set { _proImage = value; } }
    public string proFiles { get { return _proFiles; } set { _proFiles = value; } }
    public string proContent { get { return _proContent; } set { _proContent = value; } }
    public string proDetail { get { return _proDetail; } set { _proDetail = value; } }
    public string proDate { get { return _proDate; } set { _proDate = value; } }
    public string proTitle { get { return _proTitle; } set { _proTitle = value; } }
    public string proDescription { get { return _proDescription; } set { _proDescription = value; } }
    public string proKeyword { get { return _proKeyword; } set { _proKeyword = value; } }
    public string proPriority { get { return _proPriority; } set { _proPriority = value; } }
    public string proIndex { get { return _proIndex; } set { _proIndex = value; } }
    public string catId { get { return _catId; } set { _catId = value; } }
    public string proType { get { return _proType; } set { _proType = value; } }
    public string proActive { get { return _proActive; } set { _proActive = value; } }
    public string proOriginalPrice { get { return _proOriginalPrice; } set { _proOriginalPrice = value; } }
    public string proPrice { get { return _proPrice; } set { _proPrice = value; } }
    public string proUnitprice { get { return _proUnitprice; } set { _proUnitprice = value; } }
    public string proPromotions { get { return _proPromotions; } set { _proPromotions = value; } }
    public string proWeight { get { return _proWeight; } set { _proWeight = value; } }
    public string proUnits { get { return _proUnits; } set { _proUnits = value; } }
    public string proOrd { get { return _proOrd; } set { _proOrd = value; } }
    public string proLang { get { return _proLang; } set { _proLang = value; } }
    public string proWarranty { get { return _proWarranty; } set { _proWarranty = value; } }
    public string proTagName { get { return _proTagName; } set { _proTagName = value; } }
    public string proVideo { get { return _proVideo; } set { _proVideo = value; } }
    public string proCount { get { return _proCount; } set { _proCount = value; } }
    public string proCare { get { return _proCare; } set { _proCare = value; } }
    public string proSplq { get { return _proSplq; } set { _proSplq = value; } }
    public string proStatus { get { return _proStatus; } set { _proStatus = value; } }
    public string manufacturerId { get { return _manufacturerId; } set { _manufacturerId = value; } }
    #endregion
}