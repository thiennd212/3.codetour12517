﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.advertise
{
    public partial class advertiseList : System.Web.UI.UserControl
    {
        string _Admin = "";
        private string lang = "vi";
        int pageSize = 15;
        string userid = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); } if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }
            if (Session["uid"] != null) userid = Session["uid"].ToString();
            if (Request["pages"] != null && !Request["pages"].Equals(""))
            {
                pageSize = int.Parse(Request["pages"].ToString());
            }

            if (!IsPostBack)
            {
                BindCategoryPage();
                BindNewPage();
                BindManPage();
                BindLibPage();
                BindMenu();
                BindData();
            }
        }

        void BindData()
        {
            string _strWhere = " and advLang = '" + lang + "'";

            if (drlForder.SelectedValue != "0")
            {
                _strWhere += " and advPosition='" + drlForder.SelectedValue + "'";
            }

            if (txtSearch.Text != "")
            {
                _strWhere += " and ( LOWER(advName) LIKE N'%" + txtSearch.Text.ToLower() + "%')";
            }



            List<tbAdvertiseDATA> _listAdv = new List<tbAdvertiseDATA>();
            _listAdv = tbAdvertiseDB.tbAdvertise_GetByChuoi(_strWhere);

            if (_listAdv.Count() > 0)
            {
                recordCount = _listAdv.Count();
                #region Statistic
                int fResult = currentPage * pageSize + 1;
                int tResult = (currentPage + 1) * pageSize;
                tResult = tResult > recordCount ? recordCount : tResult;
                ltrStatistic.Text = "Hiển thị kết quả từ " + fResult + " - " + tResult + " trong tổng số " + recordCount + " kết quả";
                #endregion

                var result = _listAdv.Skip(currentPage * pageSize).Take(pageSize);
                rptListAdv.DataSource = result;
                rptListAdv.DataBind();
                BindPaging();
            }
            else
            {
                rptListAdv.DataSource = null;
                rptListAdv.DataBind();
            }

        }

        #region Page

        private int currentPage { get { return ViewState["currentPage"] != null ? int.Parse(ViewState["currentPage"].ToString()) : 0; } set { ViewState["currentPage"] = value; } }
        private int recordCount { get { return ViewState["recordCount"] != null ? int.Parse(ViewState["recordCount"].ToString()) : 0; } set { ViewState["recordCount"] = value; } }
        private int pageCount { get { double iCount = (double)((decimal)recordCount / (decimal)pageSize); return (int)Math.Ceiling(iCount); } }

        private void BindPaging()
        {
            int icurPage = currentPage + 1;
            int ipCount = pageCount;
            if (ipCount >= 1)
            {
                rptNumberPage.Visible = true;
                int PageShow = ipCount > 5 ? 5 : ipCount;
                int FromPage;
                int ToPage;
                DataTable dt = new DataTable();
                dt.Columns.Add("PageIndex");
                dt.Columns.Add("PageText");
                FromPage = icurPage > PageShow ? icurPage - PageShow : 1;
                ToPage = (ipCount - icurPage > PageShow) ? icurPage + PageShow : ipCount;
                if (icurPage - 10 > 0) dt.Rows.Add(icurPage - 9, icurPage - 10);
                for (int i = FromPage; i <= ToPage; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = i - 1;
                    dr[1] = i;
                    dt.Rows.Add(dr);
                }
                if (icurPage + 10 <= ipCount) dt.Rows.Add(icurPage + 9, icurPage + 10);
                rptNumberPage.DataSource = dt;
                rptNumberPage.DataBind();
            }
        }

        protected void btnPage_Click(object sender, EventArgs e)
        {
            if (((LinkButton)sender).ID == "btnPrevious")
            {
                if (currentPage > 0) currentPage = currentPage - 1;
            }
            else if (((LinkButton)sender).ID == "btnNext")
            {
                if (currentPage < pageCount - 1) currentPage = currentPage + 1;
            }
            BindData();
        }

        protected void rptNumberPage_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("page"))
            {
                currentPage = Convert.ToInt32(e.CommandArgument.ToString());
                BindData();
            }
        }

        protected void rptNumberPage_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkPage = (LinkButton)e.Item.FindControl("btn");
            Literal _ltrPage = (Literal)e.Item.FindControl("ltrLiPage");
            if (lnkPage.CommandArgument.ToString() == currentPage.ToString())
            {
                lnkPage.Enabled = false;
                _ltrPage.Text = "<li class=\"paginate_button active\">";
            }
            else
            {
                _ltrPage.Text = "<li class=\"paginate_button\">";
            }
        }

        #endregion


        protected string BindImages(string imgpath)
        {
            string _str = "";
            if (imgpath.ToString().Trim().Length > 0)
            {
                try
                {
                    if (imgpath.IndexOf(".swf") > 0)
                    {
                        _str += "<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' codebase='http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0' border='0' width='150px' height='90'>\n";
                        _str += "<param name='movie' value='" + imgpath + "'>\n";
                        _str += "<param name='AllowScriptAccess' value='always'>\n";
                        _str += "<param name='quality' value='High'>\n";
                        _str += "<embed src='" + imgpath + "'\n";
                        _str += "pluginspage='http://www.macromedia.com/go/getflashplayer'\n";
                        _str += "type='application/x-shockwave-flash'\n";
                        _str += "allowscriptaccess='always'\n";
                        _str += "width='150'\n";
                        _str += "height='90'>\n";
                        _str += "</object>\n";
                    }
                    else
                    {
                        System.Drawing.Image imgPhotoVert = System.Drawing.Image.FromFile(Server.MapPath(imgpath.ToString()));
                        int sourceWidth = imgPhotoVert.Width;
                        int sourceHeight = imgPhotoVert.Height;

                        float nPercent = 0;
                        float nPercentW = 0;
                        float nPercentH = 0;
                        if (sourceWidth > 150 || sourceHeight > 80)
                        {
                            nPercentW = ((float)150 / (float)sourceWidth);
                            nPercentH = ((float)80 / (float)sourceHeight);
                            if (nPercentH < nPercentW)
                            {
                                nPercent = nPercentH;
                            }
                            else
                            {
                                nPercent = nPercentW;
                            }
                            int destWidth = (int)(sourceWidth * nPercent);
                            int destHeight = (int)(sourceHeight * nPercent);
                            _str = "<img src=" + imgpath.ToString().Trim() + " border=0 width=" + destWidth + " height=" + destHeight + ">";

                        }
                        else
                        {
                            int destWidth = (int)(sourceWidth);
                            int destHeight = (int)(sourceHeight);
                            _str = "<img src=" + imgpath.ToString().Trim() + " border=0 width=" + destWidth + " height=" + destHeight + ">";

                        }
                    }
                }
                catch { }
            }
            return _str;
        }

        void Resetcontrol()
        {
            hidID.Value = "";
            ddlPosition.SelectedValue = "0";
            drlForder.SelectedValue = "0";
            txtTen.Text = "";
            txtDorong.Text = "0";
            txtChieucao.Text = "0";
            txtTranglienket.Text = "";
            txtNoidung.Text = "";
            txtThuTu.Text = "1";
            txtImage.Text = "";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = false;
            pnlAddForder.Visible = true;
            Resetcontrol();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            RepeaterItem item = default(RepeaterItem);
            for (int i = 0; i < rptListAdv.Items.Count; i++)
            {
                item = rptListAdv.Items[i];
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    if (((CheckBox)item.FindControl("chkBox")).Checked)
                    {
                        HiddenField hidID = (HiddenField)item.FindControl("hidCatID");
                        tbAdvertiseDB.tbAdvertise_Delete(hidID.Value);
                    }
                }
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Xóa thành công !";
            chkSelectAll.Checked = false;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void drlForder_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindData();
        }

        public static string ShowActive(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "<i class=\"fa fa-check\"></i>" : "<i class=\"fa fa-times\"></i>";
        }

        public static string ShowActiveClass(string proActive)
        {
            return proActive == "1" || proActive == "True" ? "btn btn-primary btn-xs" : "btn btn-danger btn-xs";
        }

        protected void txtName_TextChanged(object sender, EventArgs e)
        {
            TextBox txtName = (TextBox)sender;
            var lblID = (Label)txtName.FindControl("lblID");
            var obj = db.tbAdvertises.Where(t => t.advId == int.Parse(lblID.Text)).FirstOrDefault();
            if (obj != null)
            {
                obj.advName = txtName.Text;
                db.SubmitChanges();
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật tên thành công !";
        }

        protected void txtNumberWidth_TextChanged(object sender, EventArgs e)
        {
            TextBox txtNumberWidth = (TextBox)sender;
            var lblID = (Label)txtNumberWidth.FindControl("lblID");
            var obj = db.tbAdvertises.Where(t => t.advId == int.Parse(lblID.Text)).FirstOrDefault();
            if (obj != null)
            {
                obj.advWidth = Convert.ToInt32(txtNumberWidth.Text);
                db.SubmitChanges();
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật chiều rộng thành công !";
        }

        protected void txtNumberHight_TextChanged(object sender, EventArgs e)
        {
            TextBox txtNumberHight = (TextBox)sender;
            var lblID = (Label)txtNumberHight.FindControl("lblID");
            var obj = db.tbAdvertises.Where(t => t.advId == int.Parse(lblID.Text)).FirstOrDefault();
            if (obj != null)
            {
                obj.advHeight = Convert.ToInt32(txtNumberHight.Text);
                db.SubmitChanges();
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật chiều cao thành công !";
        }

        protected void txtNumberOrder_TextChanged(object sender, EventArgs e)
        {
            TextBox txtNumberOrder = (TextBox)sender;
            var lblID = (Label)txtNumberOrder.FindControl("lblID");
            var obj = db.tbAdvertises.Where(t => t.advId == int.Parse(lblID.Text)).FirstOrDefault();
            if (obj != null)
            {
                obj.advOrd = Convert.ToInt32(txtNumberOrder.Text);
                db.SubmitChanges();
            }
            BindData();
            pnlErr.Visible = true;
            ltrErr.Text = "Cập nhật thứ tự thành công !";
        }

        public void AddNewCare(int advId)
        {            
            try
            {
                var advc = db.tbAdvertise_categories.Where(a => a.advertisID == advId).ToList();
                if (advc.Count > 0){
                    db.tbAdvertise_categories.DeleteAllOnSubmit(advc);
                    db.SubmitChanges();                    
                }
                for (int k = 0; k < chkCat.Items.Count; k++)
                {
                    if (chkCat.Items[k].Selected)
                    {
                        tbAdvertise_category avi = new tbAdvertise_category
                        {
                            advertisID = advId,
                            CatID = int.Parse(chkCat.Items[k].Value)
                        };
                        db.tbAdvertise_categories.InsertOnSubmit(avi);
                        db.SubmitChanges();
                    }
                }
            }
            catch
            {}            
        }

        public void AddNewGroup(int advId)
        {
            try
            {
                var advc = db.tbAdvertise_categories.Where(a => a.advertisID == advId).ToList();
                if (advc.Count > 0)
                {
                    db.tbAdvertise_categories.DeleteAllOnSubmit(advc);
                    db.SubmitChanges();
                }
                for (int k = 0; k < chkNew.Items.Count; k++)
                {
                    if (chkNew.Items[k].Selected)
                    {
                        tbAdvertise_category avi = new tbAdvertise_category
                        {
                            advertisID = advId,
                            CatID = int.Parse(chkNew.Items[k].Value)
                        };
                        db.tbAdvertise_categories.InsertOnSubmit(avi);
                        db.SubmitChanges();
                    }
                }
            }
            catch
            { }
        }

        public void AddManGroup(int advId)
        {
            try
            {
                var advc = db.tbAdvertise_categories.Where(a => a.advertisID == advId).ToList();
                if (advc.Count > 0)
                {
                    db.tbAdvertise_categories.DeleteAllOnSubmit(advc);
                    db.SubmitChanges();
                }
                for (int k = 0; k < chkMan.Items.Count; k++)
                {
                    if (chkMan.Items[k].Selected)
                    {
                        tbAdvertise_category avi = new tbAdvertise_category
                        {
                            advertisID = advId,
                            CatID = int.Parse(chkMan.Items[k].Value)
                        };
                        db.tbAdvertise_categories.InsertOnSubmit(avi);
                        db.SubmitChanges();
                    }
                }
            }
            catch
            { }
        }

        public void AddLibGroup(int advId)
        {
            try
            {
                var advc = db.tbAdvertise_categories.Where(a => a.advertisID == advId).ToList();
                if (advc.Count > 0)
                {
                    db.tbAdvertise_categories.DeleteAllOnSubmit(advc);
                    db.SubmitChanges();
                }
                for (int k = 0; k < chkLib.Items.Count; k++)
                {
                    if (chkLib.Items[k].Selected)
                    {
                        tbAdvertise_category avi = new tbAdvertise_category
                        {
                            advertisID = advId,
                            CatID = int.Parse(chkLib.Items[k].Value)
                        };
                        db.tbAdvertise_categories.InsertOnSubmit(avi);
                        db.SubmitChanges();
                    }
                }
            }
            catch
            { }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Validation() == true)
            {
                string sadvActive = "";
                if (chkActive.Checked == true) { sadvActive = "1"; } else { sadvActive = "0"; }

                string strID = hidID.Value;

                if (strID.Length == 0)
                {
                    tbAdvertiseDATA ObjtbAdvertiseDATA = new tbAdvertiseDATA();
                    ObjtbAdvertiseDATA.advName = txtTen.Text;
                    ObjtbAdvertiseDATA.advImage = txtImage.Text;
                    ObjtbAdvertiseDATA.advWidth = txtDorong.Text;
                    ObjtbAdvertiseDATA.advHeight = txtChieucao.Text;
                    ObjtbAdvertiseDATA.advLink = txtTranglienket.Text;
                    ObjtbAdvertiseDATA.advTarget = drlKieuxuathien.SelectedValue;
                    ObjtbAdvertiseDATA.advContent = txtNoidung.Text;
                    ObjtbAdvertiseDATA.advPosition = ddlPosition.SelectedValue;
                    ObjtbAdvertiseDATA.pagId = "";
                    ObjtbAdvertiseDATA.advClick = "0";
                    ObjtbAdvertiseDATA.advOrd = txtThuTu.Text;
                    ObjtbAdvertiseDATA.advActive = sadvActive;
                    ObjtbAdvertiseDATA.advLang = lang;
                    if (tbAdvertiseDB.tbAdvertise_Add(ObjtbAdvertiseDATA))
                    {
                        int objMaxid = db.tbAdvertises.Max(s => s.advId);
                        if (ddlPosition.SelectedValue == "12")
                        {
                            AddNewCare(objMaxid);
                        }
                        else if (ddlPosition.SelectedValue == "20") {
                            AddNewMenu(objMaxid);
                        }
                        else if (ddlPosition.SelectedValue == "21")
                        {
                            AddNewGroup(objMaxid);
                        }
                        else if (ddlPosition.SelectedValue == "22") {
                            AddManGroup(objMaxid);
                        }
                        else if (ddlPosition.SelectedValue == "23") {
                            AddLibGroup(objMaxid);
                        }
                        pnModule.Visible = false;
                        pnPage.Visible = false;
                        pnlErr.Visible = true;
                        ltrErr.Text = "Thêm mới quảng cáo thành công !";
                        Resetcontrol();
                        BindData();
                        hidID.Value = "";
                        pnlListForder.Visible = true;
                        pnlAddForder.Visible = false;

                    }
                }
                else
                {
                    if (strID.Length > 0)
                    {
                        List<tbAdvertiseDATA> listadv = tbAdvertiseDB.tbAdvertise_GetByID(strID);
                        if (listadv.Count > 0)
                        {
                            listadv[0].advId = strID;
                            listadv[0].advName = txtTen.Text;
                            listadv[0].advImage = txtImage.Text;
                            listadv[0].advWidth = txtDorong.Text;
                            listadv[0].advHeight = txtChieucao.Text;
                            listadv[0].advLink = txtTranglienket.Text;
                            listadv[0].advTarget = drlKieuxuathien.SelectedValue;
                            listadv[0].advContent = txtNoidung.Text;
                            listadv[0].advPosition = ddlPosition.SelectedValue;
                            listadv[0].pagId = "";
                            listadv[0].advClick = "0";
                            listadv[0].advOrd = txtThuTu.Text;
                            listadv[0].advActive = sadvActive;
                            listadv[0].advLang = lang;
                            if (tbAdvertiseDB.tbAdvertise_Update(listadv[0]))
                            {
                                if (ddlPosition.SelectedValue == "12")
                                {
                                    AddNewCare(int.Parse(strID));
                                }
                                else if (ddlPosition.SelectedValue == "20")
                                {
                                    AddNewMenu(int.Parse(strID));
                                }
                                else if (ddlPosition.SelectedValue == "21")
                                {
                                    AddNewGroup(int.Parse(strID));
                                }
                                else if (ddlPosition.SelectedValue == "22")
                                {
                                    AddManGroup(int.Parse(strID));
                                }
                                else if (ddlPosition.SelectedValue == "23") {
                                    AddLibGroup(int.Parse(strID));
                                }
                                pnModule.Visible = false;
                                pnPage.Visible = false;
                                pnlErr.Visible = true;
                                ltrErr.Text = "Cập nhật quảng cáo thành công !";
                                Resetcontrol();
                                BindData();
                                hidID.Value = "";

                                pnlListForder.Visible = true;
                                pnlAddForder.Visible = false;
                            }
                            listadv.Clear();
                            listadv = null;
                        }
                    }
                }
            }
        }

        bool Validation()
        {
            if (ddlPosition.SelectedValue == "0")
            {
                ltrErr2.Text = "Chưa chọn vị trí quảng cáo"; ddlPosition.Focus(); pnlErr2.Visible = true; return false;
            }
            if (txtTen.Text == "")
            {
                ltrErr2.Text = "Chưa nhập tên quảng cáo"; txtTen.Focus(); pnlErr2.Visible = true; return false;
            }
            if (txtDorong.Text == "" || txtDorong.Text == "0")
            {
                ltrErr2.Text = "Chưa nhập độ rộng quảng cáo"; txtDorong.Focus(); pnlErr2.Visible = true; return false;
            }
            if (txtChieucao.Text == "" || txtChieucao.Text == "0")
            {
                ltrErr2.Text = "Chưa chiều cao rộng quảng cáo"; txtChieucao.Focus(); pnlErr2.Visible = true; return false;
            }
            if (txtTranglienket.Text == "")
            {
                ltrErr2.Text = "Chưa nhập trang liên kết quảng cáo"; txtTranglienket.Focus(); pnlErr2.Visible = true; return false;
            }
            return true;
        }

        protected void BindCategoryPage()
        {
            List<tbPage> obj = db.tbPages.Where(s => s.pagLang == lang && s.pagType == 100).ToList();
            if (obj.Count > 0)
            {
                chkCat.DataSource = obj;
                chkCat.DataBind();
            }
        }

        protected void BindNewPage()
        {
            List<tbPage> obj = db.tbPages.Where(s => s.pagLang == lang && s.pagType == int.Parse(pageType.GN)).ToList();
            if (obj.Count > 0)
            {
                chkNew.DataSource = obj;
                chkNew.DataBind();
            }
        }

        protected void BindManPage()
        {
            List<tbPage> obj = db.tbPages.Where(s => s.pagLang == lang && s.pagType == int.Parse(pageType.GM)).ToList();
            if (obj.Count > 0)
            {
                chkMan.DataSource = obj;
                chkMan.DataBind();
            }
        }

        protected void BindLibPage()
        {
            List<tbPage> obj = db.tbPages.Where(s => s.pagLang == lang && s.pagType == int.Parse(pageType.GL)).ToList();
            if (obj.Count > 0)
            {
                chkLib.DataSource = obj;
                chkLib.DataBind();
            }
        }

        protected void BindMenu()
        {
            List<tbPage> obj = db.tbPages.Where(s => s.pagLang == lang && s.pagPosition == 1 && s.pagType == int.Parse(pageType.MC) && s.paglevel.Length == 5).ToList();
            if (obj.Count > 0)
            {
                chkPage.DataSource = obj;
                chkPage.DataBind();
            }
        }

        public void AddNewMenu(int advId)
        {
            int k = 0;
            try
            {
                var advc = db.tbAdvertise_Pages.Where(a => a.advertisID == advId).ToList();
                if (advc.Count > 0)
                {
                    db.tbAdvertise_Pages.DeleteAllOnSubmit(advc);
                    db.SubmitChanges();                    
                }
                for (k = 0; k < chkPage.Items.Count; k++)
                {
                    if (chkPage.Items[k].Selected)
                    {
                        tbAdvertise_Page avi = new tbAdvertise_Page
                        {
                            advertisID = advId,
                            pageID = int.Parse(chkPage.Items[k].Value)
                        };
                        db.tbAdvertise_Pages.InsertOnSubmit(avi);
                        db.SubmitChanges();
                    }
                }
            }
            catch{}            
        }

        public void BindCategoryAdv(string catID)
        {
            int i = 0;
            List<tbAdvertise_category> advlist = db.tbAdvertise_categories.Where(a => a.advertisID == int.Parse(catID)).ToList();
            for (i = 0; i < advlist.Count; i++)
            {
                int k = 0;
                for (k = 0; k < chkCat.Items.Count; k++)
                {
                    if (chkCat.Items[k].Value == advlist[i].CatID.ToString())
                    {
                        chkCat.Items[k].Selected = true;
                    }
                }
            }
        }
        public void BindNewAdv(string newId)
        {
            int i = 0;
            List<tbAdvertise_category> advlist = db.tbAdvertise_categories.Where(a => a.advertisID == int.Parse(newId)).ToList();
            for (i = 0; i < advlist.Count; i++)
            {
                int k = 0;
                for (k = 0; k < chkNew.Items.Count; k++)
                {
                    if (chkNew.Items[k].Value == advlist[i].CatID.ToString())
                    {
                        chkNew.Items[k].Selected = true;
                    }
                }
            }
        }

        public void BindManAdv(string newId)
        {
            int i = 0;
            List<tbAdvertise_category> advlist = db.tbAdvertise_categories.Where(a => a.advertisID == int.Parse(newId)).ToList();
            for (i = 0; i < advlist.Count; i++)
            {
                int k = 0;
                for (k = 0; k < chkMan.Items.Count; k++)
                {
                    if (chkMan.Items[k].Value == advlist[i].CatID.ToString())
                    {
                        chkMan.Items[k].Selected = true;
                    }
                }
            }
        }

        public void BindLibAdv(string newId)
        {
            int i = 0;
            List<tbAdvertise_category> advlist = db.tbAdvertise_categories.Where(a => a.advertisID == int.Parse(newId)).ToList();
            for (i = 0; i < advlist.Count; i++)
            {
                int k = 0;
                for (k = 0; k < chkLib.Items.Count; k++)
                {
                    if (chkLib.Items[k].Value == advlist[i].CatID.ToString())
                    {
                        chkLib.Items[k].Selected = true;
                    }
                }
            }
        }

        public void BindCategoryMenu(string Id)
        {
            int i = 0;
            List<tbAdvertise_Page> advlist = db.tbAdvertise_Pages.Where(a => a.advertisID == int.Parse(Id)).ToList();
            for (i = 0; i < advlist.Count; i++)
            {
                int k = 0;
                for (k = 0; k < chkPage.Items.Count; k++)
                {
                    if (chkPage.Items[k].Value == advlist[i].pageID.ToString())
                    {
                        chkPage.Items[k].Selected = true;
                    }
                }
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            pnlListForder.Visible = true;
            pnlAddForder.Visible = false;
            BindData();
            Session["insert"] = "false";
            hidID.Value = "";
        }

        protected void rptListAdv_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string strID = e.CommandArgument.ToString();
            switch (e.CommandName.ToString())
            {
                case "Active":
                    List<tbAdvertiseDATA> lst = tbAdvertiseDB.tbAdvertise_GetByID(strID);
                    if (lst.Count > 0)
                    {

                        if (lst[0].advActive == "0")
                        {
                            lst[0].advActive = "1";
                        }
                        else { lst[0].advActive = "0"; }
                        tbAdvertiseDB.tbAdvertise_Update(lst[0]);
                    }
                    db.SubmitChanges();
                    BindData();
                    pnlErr.Visible = true;
                    ltrErr.Text = "Cập nhật trạng thái thành công !";
                    break;
                case "Del":
                    tbAdvertiseDB.tbAdvertise_Delete(strID);
                    pnlErr.Visible = true;
                    ltrErr.Text = "Xóa thành công nhóm chuyên mục !";
                    BindData();
                    break;
                case "Edit":
                    hidID.Value = strID;
                    List<tbAdvertiseDATA> list = tbAdvertiseDB.tbAdvertise_GetByID(strID);
                    txtTen.Text = list[0].advName;
                    txtImage.Text = list[0].advImage;
                    txtDorong.Text = list[0].advWidth;
                    txtChieucao.Text = list[0].advHeight;
                    txtTranglienket.Text = list[0].advLink;
                    drlKieuxuathien.SelectedValue = list[0].advTarget;
                    txtNoidung.Text = list[0].advContent;
                    ddlPosition.SelectedValue = list[0].advPosition;
                    txtThuTu.Text = list[0].advOrd;
                    if (list[0].advPosition == "12")
                    {                        
                        pnModule.Visible = true;
                        pnPage.Visible = false;
                        pnCatNew.Visible = false;
                        pnMan.Visible = false;
                        pnLib.Visible = false;
                        BindCategoryPage();
                        BindCategoryAdv(strID);
                    }
                    else if (list[0].advPosition == "20")
                    {                        
                        pnModule.Visible = false;
                        pnPage.Visible = true;
                        pnCatNew.Visible = false;
                        pnMan.Visible = false;
                        pnLib.Visible = false;
                        BindMenu();
                        BindCategoryMenu(strID);
                    }
                    else if (list[0].advPosition == "21")
                    {                        
                        pnModule.Visible = false;
                        pnPage.Visible = false;
                        pnCatNew.Visible = true;
                        pnMan.Visible = false;
                        pnLib.Visible = false;
                        BindNewPage();
                        BindNewAdv(strID);
                    }
                    else if (list[0].advPosition == "22") {
                        pnModule.Visible = false;
                        pnPage.Visible = false;
                        pnCatNew.Visible = false;
                        pnMan.Visible = true;
                        pnLib.Visible = false;
                        BindManPage();
                        BindManAdv(strID);
                    }
                    else if (list[0].advPosition == "23")
                    {
                        pnModule.Visible = false;
                        pnPage.Visible = false;
                        pnCatNew.Visible = false;
                        pnMan.Visible = false;
                        pnLib.Visible = true;
                        BindLibPage();
                        BindLibAdv(strID);

                    }
                    if (list[0].advActive == "1")
                    {
                        chkActive.Checked = true;
                    }
                    else
                    {
                        chkActive.Checked = false;
                    }
                    pnlErr.Visible = false;
                    ltrErr.Text = "";
                    pnlListForder.Visible = false;
                    pnlAddForder.Visible = true;
                    break;
            }
        }

        protected void ddlPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlPosition.SelectedValue == "12")
            {
                pnModule.Visible = true;
                pnPage.Visible = false;
                pnCatNew.Visible = false;
                pnMan.Visible = false;
                pnLib.Visible = false;
            }
            else if (ddlPosition.SelectedValue == "20")
            {
                pnModule.Visible = false;
                pnPage.Visible = true;                
                pnCatNew.Visible = false;
                pnMan.Visible = false;
                pnLib.Visible = false;
            }
            else if (ddlPosition.SelectedValue == "21")
            {
                pnModule.Visible = false;
                pnPage.Visible = false;
                pnCatNew.Visible = true;
                pnMan.Visible = false;
                pnLib.Visible = false;
            }
            else if (ddlPosition.SelectedValue == "22")
            {
                pnModule.Visible = false;
                pnPage.Visible = false;
                pnCatNew.Visible = false;
                pnMan.Visible = true;
                pnLib.Visible = false;
            }
            else if (ddlPosition.SelectedValue == "23")
            {
                pnModule.Visible = false;
                pnPage.Visible = false;
                pnCatNew.Visible = false;
                pnMan.Visible = false;
                pnLib.Visible = true;
            }
            else
            {
                pnModule.Visible = false;
                pnPage.Visible = false;
                pnCatNew.Visible = false;
                pnMan.Visible = false;
                pnLib.Visible = false;
            }
        }
    }
}