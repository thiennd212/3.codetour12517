﻿using MyWeb.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel.project
{
    public partial class projectAdd : System.Web.UI.UserControl
    {
        string _Admin = "";
        private string lang = "vi";
        string userid = "";
        string _strID = "";
        public bool attr = true;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLangAdm();
            _strID = PageHelper.GetQueryString("idpro");
            if (!IsPostBack)
            {
                if (Session["admin"] != null) { _Admin = Session["admin"].ToString(); }
                if (Session["uid"] != null) userid = Session["uid"].ToString();
                ddlImage.Attributes.Add("onchange", "LoadImageList();");
                BindImages();
                LoadFormSize(10);
                BindForder();
                BindTags();
                hidIDprj.Value = "";
                if (_strID != "")
                {
                    BindDataProduct(_strID);
                }
                else
                {
                    BindData();
                }
            }
        }

        void BindData()
        {
            var strTabs = "";
            strTabs += "<div id='TabsAppendprj'>";
            strTabs += "<div class=\"item_tabs\">";
            strTabs += "<div class=\"form-group\">";
            strTabs += "<label class=\"control-label col-md-2\">Tên Tab hiển thị:</label>";
            strTabs += "<div class=\"col-md-7\">";
            strTabs += "<input id=\"fieldprj0\" type=\"text\" class=\"form-control target\" value=\"Tổng quan dự án\"/>";
            strTabs += "</div>";
            strTabs += "<div class=\"col-md-3\">";
            strTabs += "<input id=\"fvitriprj0\" type=\"text\" class=\"form-control target\" onblur=\"valid(this,'quotes')\" onkeyup=\"valid(this,'quotes')\" placeholder=\"Thứ tự\" value=\"1\"/>";
            strTabs += "</div>";
            strTabs += "</div>";

            strTabs += "<div class=\"form-group\">";
            strTabs += "<label class=\"control-label col-md-2\">Nội dung:</label>";
            strTabs += "<div class=\"col-md-10\">";
            strTabs += "<textarea id=\"TabThongtinprj0\" name=\"TabThongtinprj0\" class=\"form-control target\"></textarea>";
            strTabs += "</div>";
            strTabs += "</div>";
            strTabs += "</div>";
            strTabs += "</div>";
            ltrTabBind.Text = strTabs;


            trTextKM.Attributes.Add("style", "display:none");
            trThoigianKM.Attributes.Add("style", "display:none");
            txtNewsDate.Text = string.Format("{0:MM/dd/yyyy}", DateTime.Now);
            hdCountTab.Value = 1.ToString();
        }

        void BindDataProduct(string intID)
        {
            hidIDprj.Value = intID;
            tbProject dbPro = db.tbProjects.FirstOrDefault(s => s.proId == Int32.Parse(intID));
            List<tbProjectTag> lstProTag = db.tbProjectTags.Where(s => s.proId == Int32.Parse(intID)).ToList();
            txtTen.Text = dbPro.proName;
            string[] arrImage = new string[10];
            if (dbPro.proImage != null && dbPro.proImage.Length > 0)
                arrImage = dbPro.proImage.Split(Convert.ToChar(","));
            ddlImage.SelectedValue = arrImage.Count().ToString();
            for (int i = 0; i < arrImage.Count(); i++)
            {
                TextBox txtMImage = (TextBox)rptImages.Items[i].FindControl("txtMImage");
                txtMImage.Text = arrImage[i];
            }
            txtVideo.Text = dbPro.proVideo;

            txtGiagoc.Text = dbPro.proOriginalPrice;
            txtGia.Text = dbPro.proPrice;
            txtLoaitien.Text = dbPro.proUnitprice;
            txtTieude.Text = dbPro.proTitle;
            txtDesscription.Text = dbPro.proDescription;
            txtKeyword.Text = dbPro.proKeyword;

            txtNewsDate.Text = string.Format("{0:MM/dd/yyyy}", dbPro.proDate);
            fckTomtat.Text = dbPro.proContent;
            fckBanDoViTri.Text = dbPro.proBanDoViTri;
            fckChuongTrinhBanHang.Text = dbPro.proChuongTrinhBanHang;
            fckGiaBanVaThanhToan.Text = dbPro.proGiaBanVaThanhToan;
            fckMatBang.Text = dbPro.proMatBang;
            fckTienIchHoanHao.Text = dbPro.proTienIchHoanHao;            

            txtMa.Text = dbPro.proCode;
            txtproWarranty.Text = dbPro.proWarranty;
            txtPromotions.Text = dbPro.proPromotions;
            if (dbPro.proActive == 1) { chkKichhoat.Checked = true; } else { chkKichhoat.Checked = false; }
            if (dbPro.proNoibat == 1) { chkNoibat.Checked = true; } else { chkNoibat.Checked = false; }
            if (dbPro.proBanchay == 1) { chkSpbanchay.Checked = true; } else { chkSpbanchay.Checked = false; }

            if (!string.IsNullOrEmpty(dbPro.proTungay + ""))
            {
                txtTungay.Text = string.Format("{0:MM/dd/yyyy}", dbPro.proTungay);
            }
            if (!string.IsNullOrEmpty(dbPro.proDenngay + ""))
            {
                txtDenngay.Text = string.Format("{0:MM/dd/yyyy}", dbPro.proDenngay);
            }
            if (!string.IsNullOrEmpty(dbPro.catId))
            {
                ddlForder.SelectedValue = dbPro.catId;
            }

            dbPro.proNoidungKM = txtPromotions.Text;
            var status = "0";
            if (!string.IsNullOrEmpty(dbPro.proStatus + ""))
            {
                status = dbPro.proStatus + "";
            }

            var strTabs = "";
            if (lstProTag.Any())
            {
                int i = 0;
                strTabs += "<div id='TabsAppendprj'>";
                foreach (var item in lstProTag)
                {
                    var ntnDel = "";
                    if (i > 0)
                    {
                        ntnDel = "<input class=\"btn btn-danger btn-sm ibtnDelprj\" type=\"button\" value=\"Xóa tab\" onclick=\"javascript:return confirm('Bạn có muốn xóa?');\" />";
                    }
                    strTabs += "<span class='span_tab'>";
                    strTabs += "<div class=\"item_tabs\">";
                    strTabs += "<div class=\"form-group\">";
                    strTabs += "<label class=\"control-label col-md-2\">Tên Tab hiển thị:</label>";
                    strTabs += "<div class=\"col-md-7\">";
                    strTabs += "<input id=\"fieldprj" + i + "\" type=\"text\" value=\"" + item.TagName + "\" type=\"text\" class=\"form-control target\" />";
                    strTabs += "</div>";
                    strTabs += "<div class=\"col-md-2\">";
                    strTabs += "<input id=\"fvitriprj" + i + "\" type=\"text\" value=\"" + item.Ord + "\" type=\"text\" class=\"form-control target\" onblur=\"valid(this,'quotes')\" onkeyup=\"valid(this,'quotes')\" placeholder=\"Thứ tự\" />";
                    strTabs += "</div>";

                    strTabs += "<div class=\"col-md-1\">";
                    strTabs += ntnDel;
                    strTabs += "</div>";

                    strTabs += "</div>";

                    strTabs += "<div class=\"form-group\">";
                    strTabs += "<label class=\"control-label col-md-2\">Nội dung:</label>";
                    strTabs += "<div class=\"col-md-10\">";
                    strTabs += "<textarea id=\"TabThongtinprj" + i + "\" name=\"TabThongtinprj" + i + "\" class=\"form-control target\">" + item.TagValue + "</textarea>";
                    strTabs += "</div>";
                    strTabs += "</div>";
                    strTabs += "</div>";
                    strTabs += "</span>";

                    i++;
                }
                strTabs += "</div>";
            }
            else
            {

                strTabs += "<div id='TabsAppendprj'>";
                strTabs += "<div class=\"item_tabs\">";
                strTabs += "<div class=\"form-group\">";
                strTabs += "<label class=\"control-label col-md-2\">Tên Tab hiển thị:</label>";
                strTabs += "<div class=\"col-md-7\">";
                strTabs += "<input id=\"fieldprj0\" type=\"text\" class=\"form-control target\" />";
                strTabs += "</div>";
                strTabs += "<div class=\"col-md-3\">";
                strTabs += "<input id=\"fvitriprj0\" type=\"text\" class=\"form-control target\" onblur=\"valid(this,'quotes')\" onkeyup=\"valid(this,'quotes')\" placeholder=\"Thứ tự\" />";
                strTabs += "</div>";
                strTabs += "</div>";

                strTabs += "<div class=\"form-group\">";
                strTabs += "<label class=\"control-label col-md-2\">Nội dung:</label>";
                strTabs += "<div class=\"col-md-10\">";
                strTabs += "<textarea id=\"TabThongtinprj0\" name=\"TabThongtinprj\" class=\"form-control target\"></textarea>";
                strTabs += "</div>";
                strTabs += "</div>";
                strTabs += "</div>";
                strTabs += "</div>";
            }
            ltrTabBind.Text = strTabs;

            lstNewRelate.Items.Clear();
            if (dbPro.proTag != null)
            {
                int i = 0;
                string strMaTin = null;
                strMaTin = dbPro.proTag;
                string[] MaTin = null;
                MaTin = strMaTin.Split(new Char[] { ';' });
                string k = null;
                lstB.Items.Clear();
                foreach (string k_loopVariable in MaTin)
                {
                    k = k_loopVariable;
                    if (!string.IsNullOrEmpty(k.Trim()))
                    {
                        lstB.Items.Add(new ListItem(k, k));
                    }
                }

                for (i = 0; i < lstB.Items.Count; i++)
                {

                    IEnumerable<tbTag> list2 = db.tbTags.Where(s => s.Lang == lang && s.Active == 1 && s.Type == 3);
                    foreach (tbTag n in list2)
                    {
                        if (lstB.Items[i].Text == n.Name.ToString())
                        {
                            lstNewRelate.Items.Add(new ListItem(n.Name.ToString(), n.Id.ToString()));
                        }
                    }
                }
            }

        }

        private void BindImages()
        {
            ddlImage.Items.Clear();
            for (int i = 1; i <= 10; i++)
            {
                ddlImage.Items.Add(new ListItem(i.ToString() + " ảnh", i.ToString()));
            }
            ddlImage.DataBind();
        }

        protected void LoadFormSize(int intSize)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add("Id");
            for (int i = 0; i < intSize; i++)
            {
                System.Data.DataRow dr = dt.NewRow();
                dr["Id"] = i + 1;
                dt.Rows.Add(dr);
            }
            rptImages.DataSource = dt.DefaultView;
            rptImages.DataBind();
        }

        protected DateTime convertDate(string date)
        {
            DateTime datetime = DateTime.Now;
            if (date != "")
            {
                var arrDate = date.Split(' ');
                string strNewTime = arrDate[1];
                if (arrDate[2] == "PM")
                {
                    var hour = int.Parse(arrDate[1].Split(':')[0].ToString());
                    hour = hour + 12;
                    strNewTime = hour + ":" + arrDate[1].Split(':')[1];
                }
                string strNewDate = arrDate[0] + " " + strNewTime + ":00";
                datetime = DateTime.ParseExact(strNewDate, "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat);
            }
            return datetime;
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validation() == true)
                {
                    DateTime datetime = DateTime.Now;
                    if (txtNewsDate.Text != "")
                    {
                        datetime = convertDate(txtNewsDate.Text);
                    }

                    string sproIndex = "0";
                    string sproActive = "0";
                    int iImage = int.Parse(ddlImage.SelectedValue);
                    string[] arrImage = new string[iImage];
                    for (int i = 0; i < iImage; i++)
                    {
                        TextBox txtMImage = (TextBox)rptImages.Items[i].FindControl("txtMImage");
                        if (!String.IsNullOrEmpty(txtMImage.Text))
                            arrImage[i] = txtMImage.Text;
                    }

                    arrImage = arrImage.Where(s => s != null && s.Length > 0).ToArray();
                    if (chkKichhoat.Checked == true) { sproActive = "1"; } else { sproActive = "0"; }
                    string proID = hidIDprj.Value;
                    if (proID.Length == 0)
                    {
                        var objProject = new tbProject();
                        objProject.proCode = txtMa.Text.Trim();
                        objProject.proName = txtTen.Text.Trim();
                        objProject.proImage = string.Join(",", arrImage);
                        objProject.proFiles = "";
                        objProject.proContent = fckTomtat.Text;
                        objProject.proDetail = "";
                        objProject.proDate = datetime;
                        objProject.proTitle = txtTieude.Text;
                        objProject.proDescription = txtDesscription.Text;
                        objProject.proKeyword = txtKeyword.Text;
                        objProject.proPriority = 0;
                        objProject.proIndex = Convert.ToInt32(sproIndex);
                        objProject.proType = 1;
                        objProject.proActive = Convert.ToInt32(sproActive);
                        objProject.proOriginalPrice = txtGiagoc.Text.Trim() == "" ? "0" : txtGiagoc.Text;
                        objProject.proPrice = txtGia.Text.Trim() == "" ? "0" : txtGia.Text;
                        objProject.proUnitprice = txtLoaitien.Text;
                        objProject.proPromotions = txtPromotions.Text;
                        objProject.proWeight = "0";
                        objProject.proUnits = "";
                        objProject.proLang = lang;
                        objProject.proWarranty = txtproWarranty.Text;
                        objProject.proTagName = "";
                        objProject.proVideo = txtVideo.Text;
                        objProject.proSpLienQuan = "";
                        objProject.proSpLienQuan = "";
                        objProject.proUrl = "";
                        objProject.proKieuspdaban = 0;
                        objProject.proNoibat = chkNoibat.Checked ? 1 : 0;
                        objProject.proBanchay = chkSpbanchay.Checked ? 1 : 0;

                        /*Extend*/
                        objProject.proTongQuan = fckTomtat.Text;
                        objProject.proMatBang = fckMatBang.Text;
                        objProject.proChuongTrinhBanHang = fckChuongTrinhBanHang.Text;
                        objProject.proGiaBanVaThanhToan = fckGiaBanVaThanhToan.Text;
                        objProject.proBanDoViTri = fckBanDoViTri.Text;
                        objProject.proTienIchHoanHao = fckTienIchHoanHao.Text;
                        /*End*/

                        if (!string.IsNullOrEmpty(txtTungay.Text))
                        {
                            objProject.proTungay = convertDate(txtTungay.Text);
                        }
                        if (!string.IsNullOrEmpty(txtDenngay.Text))
                        {
                            objProject.proDenngay = convertDate(txtDenngay.Text);
                        }
                        objProject.proNoidungKM = txtPromotions.Text;

                        objProject.catId = ddlForder.SelectedValue;
                        objProject.manufacturerId = null;
                        objProject.userId = Session["uid"].ToString();

                        #region Them vao tbPage + tbProduct
                        tbPageDATA objNewMenu = new tbPageDATA();
                        objNewMenu.pagName = txtTen.Text;
                        objNewMenu.pagImage = string.Join(",", arrImage); ;
                        objNewMenu.pagType = pageType.PDJ;
                        var curNewType = db.tbPages.Where(s => s.pagId == int.Parse(ddlForder.SelectedValue)).FirstOrDefault();
                        objNewMenu.paglevel = curNewType.paglevel + "00000";
                        objNewMenu.pagTitle = txtTen.Text;
                        objNewMenu.pagDescription = txtDesscription.Text;
                        objNewMenu.pagKeyword = txtKeyword.Text;
                        objNewMenu.pagOrd = "0";
                        objNewMenu.pagActive = chkKichhoat.Checked ? "0" : "1";
                        objNewMenu.pagLang = lang;
                        objNewMenu.pagDetail = "";
                        objNewMenu.pagLink = "";
                        objNewMenu.pagTarget = "";
                        objNewMenu.pagPosition = "0";
                        objNewMenu.catId = "";
                        objNewMenu.grnId = "";
                        objNewMenu.pagTagName = "";
                        objNewMenu.check1 = "";
                        objNewMenu.check2 = "";
                        objNewMenu.check3 = "";
                        objNewMenu.check4 = "";
                        objNewMenu.check5 = "";
                        string tagName = MyWeb.Common.StringClass.NameToTag(txtTen.Text);
                        if (tbPageDB.tbPage_Add(objNewMenu))
                        {
                            List<tbPageDATA> curItem = tbPageDB.tbPage_GetByMax();
                            var hasTagName = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault();
                            curItem[0].pagId = curItem[0].pagId;
                            curItem[0].pagTagName = hasTagName != null ? tagName + "-prj-" + curItem[0].pagId : tagName;
                            tbPageDB.tbPage_Update(curItem[0]);

                            objProject.proTagName = curItem[0].pagTagName;
                            db.tbProjects.InsertOnSubmit(objProject);
                            db.SubmitChanges();
                            var projTop = db.tbProjects.OrderByDescending(x=>x.proId).FirstOrDefault();

                            try
                            {
                                string proid = projTop.proId.ToString();

                                tbProductDB.tbProductTag("update tbProTag set proId=" + projTop.proId + " where proId is null");
                            }
                            catch
                            {
                                pnlErr.Visible = true;
                                ltrErr.Text = "Đã xảy ra lỗi ! Xin vui lòng kiểm tra dữ liệu và thử lại !";
                            }

                            pnlErr.Visible = true;
                            ltrErr.Text = "Thêm mới dự án thành công !!";
                        }
                        #endregion

                        Resetcontrol();
                        ResetUploadForm();
                        Response.Redirect("/admin-project/list.aspx");
                    }
                    else
                    {
                        string idpage = "";
                        string NewRelateID = "", strDauPhay = "";
                        int j = 0;
                        if (lstNewRelate.Items.Count > 0)
                        {
                            for (j = 0; j < lstNewRelate.Items.Count; j++)
                            {
                                strDauPhay = j == lstNewRelate.Items.Count - 1 ? "" : ";";
                                NewRelateID += lstNewRelate.Items[j].Text + strDauPhay;
                            }
                        }

                        var objUpdate = db.tbProjects.FirstOrDefault(x => x.proId == Convert.ToInt32(proID));
                        if (objUpdate != null)
                        {
                            objUpdate.proCode = txtMa.Text;
                            objUpdate.proName = txtTen.Text;
                            objUpdate.proTag = NewRelateID;
                            objUpdate.proImage = string.Join(",", arrImage);
                            objUpdate.proFiles = "";
                            objUpdate.proContent = fckTomtat.Text;
                            objUpdate.proDetail = "";
                            objUpdate.proDate = datetime;
                            objUpdate.proTitle = txtTieude.Text;
                            objUpdate.proDescription = txtDesscription.Text;
                            objUpdate.proKeyword = txtKeyword.Text;
                            objUpdate.proPriority = 0;
                            objUpdate.proIndex = Convert.ToInt32(sproIndex);
                            objUpdate.proType = 1;
                            objUpdate.proActive = Convert.ToInt32(sproActive);
                            objUpdate.proOriginalPrice = txtGiagoc.Text.Trim() == "" ? "0" : txtGiagoc.Text;
                            objUpdate.proPrice = txtGia.Text.Trim() == "" ? "0" : txtGia.Text;
                            objUpdate.proUnitprice = txtLoaitien.Text;
                            objUpdate.proPromotions = txtPromotions.Text;
                            objUpdate.proWeight = "0";
                            objUpdate.proUnits = "";
                            objUpdate.proLang = lang;
                            objUpdate.proWarranty = txtproWarranty.Text;
                            objUpdate.proVideo = txtVideo.Text;
                            objUpdate.proSpLienQuan = "";
                            objUpdate.proUrl = "";
                            objUpdate.proKieuspdaban = 0;
                            objUpdate.proNoibat = chkNoibat.Checked ? 1 : 0;
                            objUpdate.proBanchay = chkSpbanchay.Checked ? 1 : 0;

                            /*Extend*/
                            objUpdate.proTongQuan = fckTomtat.Text;
                            objUpdate.proMatBang = fckMatBang.Text;
                            objUpdate.proChuongTrinhBanHang = fckChuongTrinhBanHang.Text;
                            objUpdate.proGiaBanVaThanhToan = fckGiaBanVaThanhToan.Text;
                            objUpdate.proBanDoViTri = fckBanDoViTri.Text;
                            objUpdate.proTienIchHoanHao = fckTienIchHoanHao.Text;
                            /*End*/

                            if (!string.IsNullOrEmpty(txtTungay.Text + ""))
                            {
                                objUpdate.proTungay = convertDate(txtTungay.Text);
                            }
                            if (!string.IsNullOrEmpty(txtDenngay.Text + ""))
                            {
                                objUpdate.proDenngay = convertDate(txtDenngay.Text);
                            }
                            objUpdate.proNoidungKM = txtPromotions.Text;
                            objUpdate.catId = ddlForder.SelectedValue;
                            objUpdate.userId = Session["uid"].ToString();
                            #region Update Page
                            List<tbPageDATA> curPage = tbPageDB.tbPage_GetByTagName(objUpdate.proTagName);
                            if (curPage.Count() > 0)
                            {
                                idpage = curPage[0].pagId;
                                string bfTagname = curPage[0].pagTagName;
                                curPage[0].pagName = txtTen.Text;
                                curPage[0].pagImage = string.Join(",", arrImage);
                                curPage[0].pagType = pageType.PDJ;
                                var curNewType = db.tbPages.Where(s => s.pagId == int.Parse(ddlForder.SelectedValue)).FirstOrDefault();
                                curPage[0].paglevel = curNewType.paglevel + "00000";
                                curPage[0].pagTitle = txtTieude.Text;
                                curPage[0].pagDescription = txtDesscription.Text;
                                curPage[0].pagKeyword = txtKeyword.Text;
                                curPage[0].pagOrd = "0";
                                curPage[0].pagActive = chkKichhoat.Checked ? "0" : "1";
                                curPage[0].pagLang = lang;
                                curPage[0].pagDetail = "";
                                curPage[0].pagLink = "";
                                curPage[0].pagTarget = "";
                                curPage[0].pagPosition = "0";
                                curPage[0].catId = "0";
                                curPage[0].grnId = "0";
                                curPage[0].pagTagName = "";
                                curPage[0].check1 = "";
                                curPage[0].check2 = "";
                                curPage[0].check3 = "";
                                curPage[0].check4 = "";
                                curPage[0].check5 = "";

                                string tagName = objUpdate.proTagName;
                                List<tbPage> hasTagName = db.tbPages.Where(s => s.pagName == txtTen.Text).ToList();
                                if (hasTagName.Count > 1)
                                {
                                    curPage[0].pagTagName = MyWeb.Common.StringClass.NameToTag(txtTen.Text) + "-prj-" + objUpdate.proId;
                                }
                                else
                                {
                                    curPage[0].pagTagName = tagName;
                                }

                                objUpdate.proTagName = curPage[0].pagTagName;
                                tbPageDB.tbPage_Update(curPage[0]);

                                #region Update các menu liên kết đến
                                List<tbPageDATA> lstUpdate = tbPageDB.tbPage_GetByAll(lang);
                                lstUpdate = lstUpdate.Where(s => s.pagLink == bfTagname).ToList();
                                if (lstUpdate.Count > 0)
                                {
                                    for (int i = 0; i < lstUpdate.Count; i++)
                                    {
                                        lstUpdate[i].pagLink = curPage[0].pagTagName;
                                        tbPageDB.tbPage_Update(lstUpdate[i]);
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                tbPageDATA objNewMenu = new tbPageDATA();
                                objNewMenu.pagName = txtTen.Text;
                                objNewMenu.pagImage = "";
                                objNewMenu.pagType = pageType.PDJ;
                                var curNewType = db.tbPages.Where(s => s.pagId == int.Parse(ddlForder.SelectedValue)).FirstOrDefault();
                                objNewMenu.paglevel = curNewType.paglevel + "00000";
                                objNewMenu.pagTitle = txtTen.Text;
                                objNewMenu.pagDescription = txtDesscription.Text;
                                objNewMenu.pagKeyword = txtKeyword.Text;
                                objNewMenu.pagOrd = "0";
                                objNewMenu.pagActive = chkKichhoat.Checked ? "0" : "1";
                                objNewMenu.pagLang = lang;
                                objNewMenu.pagDetail = "";
                                objNewMenu.pagLink = "";
                                objNewMenu.pagTarget = "";
                                objNewMenu.pagPosition = "0";
                                objNewMenu.catId = "";
                                objNewMenu.grnId = "";
                                objNewMenu.pagTagName = "";
                                objNewMenu.check1 = "";
                                objNewMenu.check2 = "";
                                objNewMenu.check3 = "";
                                objNewMenu.check4 = "";
                                objNewMenu.check5 = "";
                                string tagName = MyWeb.Common.StringClass.NameToTag(txtTen.Text);
                                if (tbPageDB.tbPage_Add(objNewMenu))
                                {
                                    List<tbPageDATA> curItem = tbPageDB.tbPage_GetByMax();
                                    var hasTagName = db.tbPages.Where(s => s.pagTagName == tagName).FirstOrDefault();
                                    curItem[0].pagId = curItem[0].pagId;
                                    curItem[0].pagTagName = hasTagName != null ? tagName + "-prj-" + curItem[0].pagId : tagName;
                                    tbPageDB.tbPage_Update(curItem[0]);
                                    objUpdate.proTagName = curItem[0].pagTagName;
                                    tbPageDB.tbPage_Update(curItem[0]);
                                }
                            }
                            #endregion
                            db.SubmitChanges();
                            pnlErr.Visible = true;
                            ltrErr.Text = "Cập nhật dự án thành công !!";
                        }

                        Response.Redirect("/admin-project/list.aspx");

                    }
                }
            }
            catch (Exception ex)
            {
                ltrErr.Text = "Có lỗi xảy ra: " + ex.InnerException.Message;
            }
        }

        protected void BindTags()
        {
            IEnumerable<tbTag> list = db.tbTags.Where(s => s.Lang == lang && s.Active == 1 && s.Type == 3).OrderBy(s => s.Ord).ThenByDescending(s => s.Id);

            foreach (tbTag n in list)
            {
                lstNew.Items.Add(new ListItem(n.Name, n.Id.ToString()));
            }
        }

        void BindForder()
        {
            string strForder = "";
            if (_Admin == "0" || _Admin == "100" || _Admin == "1")
            {
                List<tbPageDATA> list = tbPageDB.tbPage_GetByAll(lang);
                list = list.Where(s => s.pagType == pageType.GPJ).ToList();
                ddlForder.Items.Clear();
                ddlForder.Items.Add(new ListItem("- Nhóm dự án -", "0"));
                for (int i = 0; i < list.Count; i++)
                {
                    strForder = "";
                    for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                    {
                        strForder = strForder + "---";
                    }
                    ddlForder.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
                }
                list.Clear();
                list = null;

            }
            else
            {
                List<tbPageDATA> list = tbPageDB.tbPage_GetByTop("", "pagType = '100' and pagId in (select roleidmodule from tbUserRole where roleuserid=" + userid + ")", "paglevel");
                ddlForder.Items.Clear();
                ddlForder.Items.Add(new ListItem("- Nhóm dự án -", "0"));
                for (int i = 0; i < list.Count; i++)
                {
                    strForder = "";
                    for (int j = 1; j < list[i].paglevel.Length / 5; j++)
                    {
                        strForder = strForder + "---";
                    }
                    ddlForder.Items.Add(new ListItem(strForder.ToString() + list[i].pagName, list[i].pagId));
                }
                list.Clear();
                list = null;
            }

        }

        #region[Validation]
        protected bool Validation()
        {
            if (ddlForder.SelectedValue == "") { ltrErr.Text = "Chưa chọn nhóm dự án !!"; pnlErr.Visible = true; return false; }
            if (txtTen.Text == "") { ltrErr.Text = "Chưa nhập tên dự án !!"; pnlErr.Visible = true; return false; }
            return true;
        }
        #endregion

        protected void btnActive_Click(object sender, EventArgs e)
        {
            int i = 0;
            for (i = 0; i <= lstNewRelate.Items.Count - 1; i++)
            {
                if (lstNewRelate.Items[i].Value == lstNew.SelectedValue)
                {
                    return;
                }
            }
            lstNewRelate.Items.Add(new ListItem(lstNew.SelectedItem.Text, lstNew.SelectedItem.Value));
        }

        protected void btnUnactive_Click(object sender, EventArgs e)
        {
            int i = 0;
            for (i = 0; i <= lstNewRelate.Items.Count - 1; i++)
            {
                if (lstNewRelate.Items[i].Selected == true)
                {
                    lstNewRelate.Items.RemoveAt(lstNewRelate.SelectedIndex);
                    return;
                }
            }
        }

        #region[Resetcontrol]
        void Resetcontrol()
        {
            ddlImage.SelectedValue = "1";
            txtTen.Text = "";
            txtGiagoc.Text = "";
            txtGia.Text = "";
            txtTieude.Text = "";
            txtDesscription.Text = "";
            txtKeyword.Text = "";
            txtPromotions.Text = "";
            fckTomtat.Text = "";
            txtMa.Text = "";
            txtproWarranty.Text = "";
            chkNoibat.Checked = false;
            chkSpbanchay.Checked = false;
            txtTungay.Text = "";
            txtDenngay.Text = "";
            txtDenngay.Text = "";
            txtPromotions.Text = "";
            lstNewRelate.Items.Clear();
        }
        protected void ResetUploadForm()
        {
            ddlImage.SelectedValue = "1";
            for (int i = 0; i < 10; i++)
            {
                TextBox txtMImage = (TextBox)rptImages.Items[i].FindControl("txtMImage");
                txtMImage.Text = "";
            }
        }
        #endregion

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("/admin-project/list.aspx");
        }

    }
}