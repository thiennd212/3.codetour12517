﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.control.panel
{
    public partial class changeLang : System.Web.UI.Page
    {
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["page"] != null)
            {
                if (Request["page"].ToString() == "control.panel")
                {
                    if (Request["langcode"] != null)
                    {
                        try
                        {
                            Session["LangAdm"] = Request["langcode"].ToString();
                            MyWeb.Global.LoadConfig(Session["LangAdm"].ToString());
                            var langname = db.tbLanguages.Where(l => l.lanId == Request["langcode"].ToString()).Select(s => s.lanName);
                            Session["lanname"] = langname.FirstOrDefault();
                        }
                        catch
                        {
                            Session["LangAdm"] = "vi";
                            MyWeb.Global.LoadConfig(Session["LangAdm"].ToString());
                            Session["lanname"] = "Viet Nam";
                        }
                    }
                    else
                    {
                        Session["LangAdm"] = "vi";
                        MyWeb.Global.LoadConfig(Session["LangAdm"].ToString());
                    }
                    Response.Redirect("/control.panel/");
                }
                else
                {
                    if (Request["langcode"] != null)
                    {
                        Session["Lang"] = Request["langcode"].ToString();
                        MyWeb.Global.LoadConfig(Session["Lang"].ToString());
                    }
                    else
                    {
                        Session["Lang"] = "vi";
                        MyWeb.Global.LoadConfig(Session["Lang"].ToString());
                    }
                    Response.Redirect("/");
                }
            }
        }
    }
}