﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="online.ascx.cs" Inherits="MyWeb.control.panel.website.online" %>

<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Quản lý tư vấn online</li>
    </ol>
    <!-- end breadcrumb -->
    <asp:Panel ID="pnlListForder" runat="server" Visible="true">
        <h1 class="page-header">Quản lý tư vấn online</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Danh sách tư vấn online</h4>
                    </div>

                    <div class="alert alert-info fade in" id="pnlErr" runat="server" visible="false">
                        <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        <button class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <asp:LinkButton ID="btnAddNew" runat="server" class="btn btn-success btn-sm" OnClick="btnAddNew_Click"><i class="fa fa-plus"></i><span>Thêm mới</span></asp:LinkButton>
                                <asp:LinkButton ID="btnDeleteAll" runat="server" class="btn btn-danger btn-sm" OnClick="btnDeleteAll_Click"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                            </div>

                            <div class="col-sm-7">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                        <thead>
                                            <tr>
                                                <th width="10">
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="False"></asp:CheckBox>
                                                </th>
                                                <th>Tên nhân viên</th>
                                                <th width="120">Điện thoại</th>
                                                <th width="130">Nick Zalo</th>
                                                <th width="130">Nick Skype</th>
                                                <th width="100">Nhóm</th>
                                                <th width="50">Sắp xếp</th>
                                                <th width="20"></th>
                                                <th width="100">Công cụ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptFolderList" runat="server" OnItemCommand="rptFolderList_ItemCommand">
                                                <ItemTemplate>
                                                    <tr class="even gradeC">
                                                        <td>
                                                            <asp:CheckBox ID="chkBox" CssClass="chkBoxSelect" runat="server"></asp:CheckBox>
                                                            <asp:HiddenField ID="hidCatID" Value='<%#DataBinder.Eval(Container.DataItem, "onId")%>' runat="server" />
                                                            <asp:Label ID="lblID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"onId")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNameForder" runat="server" Text='<%#Eval("onName1").ToString()%>' AutoPostBack="true" OnTextChanged="txtNameForder_TextChanged" class="form-control input-sm"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtTell" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "onTell")%>' onblur="valid(this,'quotes')" onkeyup="valid(this,'quotes')" OnTextChanged="txtTell_TextChanged" AutoPostBack="true" class="form-control input-sm"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtONnick" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "onYahoo")%>' OnTextChanged="txtONnick_TextChanged" AutoPostBack="true" class="form-control input-sm"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtSkype" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "onSkype")%>' OnTextChanged="txtSkype_TextChanged" AutoPostBack="true" class="form-control input-sm"></asp:TextBox>
                                                        </td>
                                                        <td><%#BindCate(DataBinder.Eval(Container.DataItem, "onType").ToString())%></td>

                                                        <td>
                                                            <asp:TextBox ID="txtNumberOrder" runat="server" class="form-control input-sm" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')" Text='<%#DataBinder.Eval(Container.DataItem, "onOrd").ToString()%>' OnTextChanged="txtNumberOrder_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="btnActive" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"onId")%>' CommandName="Active" class="btn btn-primary btn-xs" ToolTip="Kích hoạt"><%#ShowActive(DataBinder.Eval(Container.DataItem, "onActive").ToString())%></asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton class="btn btn-success btn-xs" ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"onId")%>' CommandName="Edit" ToolTip="Sửa"><i class="fa fa-pencil-square-o"></i>Sửa</asp:LinkButton>
                                                            <asp:LinkButton class="btn btn-danger btn-xs" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"onId")%>' CommandName="Del" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <div class="row dataTables_wrapper">
                            <div class="col-sm-5">
                                <div id="data-table_info" class="dataTables_info" role="status" aria-live="polite">
                                    <asp:Literal ID="ltrStatistic" runat="server"></asp:Literal>
                                </div>
                            </div>

                            <div class="col-sm-7">
                                <div id="data-table_paginate" class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <li id="data-table_previous" class="paginate_button previous disabled">
                                            <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Trước</asp:LinkButton>
                                        </li>
                                        <asp:Repeater ID="rptNumberPage" runat="server" OnItemCommand="rptNumberPage_ItemCommand" OnItemDataBound="rptNumberPage_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrLiPage" runat="server"></asp:Literal>
                                                <asp:LinkButton ID="btn" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="page" Text='<%# Eval("PageText") %> '></asp:LinkButton></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li id="data-table_next" class="paginate_button next">
                                            <asp:LinkButton ID="btnNext" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Sau</asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>



                </div>
            </div>

        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAddForder" runat="server" Visible="false">
        <h1 class="page-header">Thêm/sửa nhóm tư vấn online</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-2">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Thêm/sửa nhóm tư vấn online</h4>
                    </div>

                    <div class="panel-body panel-form">
                        <asp:Panel ID="pnlErr2" runat="server" Visible="false">
                            <div class="alert alert-danger fade in" style="border-radius: 0px;">
                                <button class="close" data-dismiss="alert" type="button">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <asp:Literal ID="ltrErr2" runat="server"></asp:Literal>
                            </div>
                        </asp:Panel>
                        <div class="form-horizontal form-bordered">

                            <div class="form-group">
                                <label class="control-label col-md-2">Nhóm tư vấn:</label>
                                <div class="col-md-7">
                                    <asp:HiddenField ID="hidID" runat="server" />
                                    <asp:DropDownList ID="drlKieu" runat="server" class="form-control"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group" style="display: none;">
                                <label class="control-label col-md-2">Kiểu tư vấn:</label>
                                <div class="col-md-7">
                                    <asp:DropDownList ID="ddlType" runat="server" class="form-control">
                                        <asp:ListItem Selected="True">Zalo</asp:ListItem>
                                        <asp:ListItem>Skype</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Tên nhân viên:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtNhanvien" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Điện thoại:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtDienthoai" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Nickname Zalo:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtNickZalo" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Nickname skype:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtNickSkype" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Email:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group" style="display: none;">
                                <label class="control-label col-md-2">Facebook:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtFace" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group" style="display: none;">
                                <label class="control-label col-md-2">Zallo:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtZalo" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Thứ tự:</label>
                                <div class="col-md-7">
                                    <asp:TextBox ID="txtThuTu" runat="server" class="form-control" onkeyup="valid(this,'quotes')" onblur="valid(this,'quotes')">1</asp:TextBox>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2">Kích hoạt:</label>
                                <div class="col-md-7">
                                    <asp:CheckBox ID="chkKichhoat" runat="server" Checked="True" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-2"></label>
                                <div class="col-md-7">
                                    <asp:LinkButton ID="btnUpdate" runat="server" class="btn btn-primary" OnClick="btnUpdate_Click">Cập nhật</asp:LinkButton>
                                    <asp:LinkButton ID="btnReset" runat="server" class="btn btn-danger" OnClick="btnReset_Click">Hủy</asp:LinkButton>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</div>
