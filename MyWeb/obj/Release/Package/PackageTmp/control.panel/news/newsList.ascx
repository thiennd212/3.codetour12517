﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="newsList.ascx.cs" Inherits="MyWeb.control.panel.news.newsList" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<script src="../../scripts/ckfinder/ckfinder.js" type="text/javascript"></script>
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
        <li><a href="/control.panel/">Trang chủ</a></li>
        <li class="active">Quản lý bài viết</li>
    </ol>
    <asp:Panel ID="pnlListForder" runat="server" Visible="true">
        <h1 class="page-header">Quản lý bài viết</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="table-basic-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Danh sách bài viết</h4>
                    </div>
                    <div class="alert alert-info fade in" id="pnlErr" runat="server" visible="false">
                        <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
                        <button class="close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3">
                                <asp:LinkButton ID="btnAddNew" runat="server" class="btn btn-success btn-sm" OnClick="btnAddNew_Click"><i class="fa fa-plus"></i><span>Thêm mới</span></asp:LinkButton>
                                <asp:LinkButton ID="btnDeleteAll" runat="server" class="btn btn-danger btn-sm" OnClick="btnDeleteAll_Click" OnClientClick="javascript:return confirm('Bạn có muốn xóa bài viết đã chọn?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                            </div>

                            <div class="col-sm-3">
                                <asp:TextBox ID="txtSearch" runat="server" class="form-control input-sm" placeholder="Từ khóa tìm kiếm"></asp:TextBox>
                            </div>
                            <div class="col-sm-2">
                                <asp:DropDownList ID="ddlType" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                                    <asp:ListItem Value="0">- Chọn kiểu -</asp:ListItem>
                                    <asp:ListItem Value="1">Tin dịch vụ</asp:ListItem>
                                    <asp:ListItem Value="2">Tin sự kiện</asp:ListItem>
                                    <asp:ListItem Value="3">Tin nổi bật</asp:ListItem>
                                    <asp:ListItem Value="4">Giới thiệu trang chủ</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-sm-2">
                                <asp:DropDownList ID="drlForder" class="form-control input-sm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drlForder_SelectedIndexChanged"></asp:DropDownList>
                            </div>

                            <div class="col-sm-1">
                                <asp:LinkButton ID="btnSearch" runat="server" class="btn btn-primary btn-sm" Style="float: right;" OnClick="btnSearch_Click"><i class="fa fa-search"></i><span>Tìm kiếm</span></asp:LinkButton>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered dataTable no-footer dtr-inline">
                                        <thead>
                                            <tr>
                                                <th width="10">
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="False"></asp:CheckBox>
                                                </th>
                                                <th>Tiêu đề bài viết</th>
                                                <th width="170">Nhóm tin</th>
                                                <th width="120">Ngày đăng</th>
                                                <th width="20"></th>
                                                <th width="160">Công cụ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptListNews" runat="server" OnItemCommand="rptListNews_ItemCommand">
                                                <ItemTemplate>
                                                    <tr class="even gradeC">
                                                        <td>
                                                            <asp:CheckBox ID="chkBox" CssClass="chkBoxSelect" runat="server"></asp:CheckBox>
                                                            <asp:HiddenField ID="hidCatID" Value='<%#DataBinder.Eval(Container.DataItem, "newid")%>' runat="server" />
                                                            <asp:Label ID="lblID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"newid")%>'></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtName" runat="server" Text='<%#Eval("newName").ToString()%>' AutoPostBack="true" OnTextChanged="txtName_TextChanged" class="form-control input-sm"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <%#BindCateName(DataBinder.Eval(Container.DataItem, "grnID").ToString())%>
                                                        </td>
                                                        <td>
                                                            <%#FormatDate(DataBinder.Eval(Container.DataItem, "newDate").ToString())%>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="btnActive" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"newid")%>' CommandName="Active" class='<%#ShowActiveClass(DataBinder.Eval(Container.DataItem, "newActive").ToString())%>' ToolTip="Kích hoạt"><%#ShowActive(DataBinder.Eval(Container.DataItem, "newActive").ToString())%></asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="btnCopy" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"newid")%>' CommandName="Copy" class="btn btn-primary btn-xs" ToolTip="Nhân bản tin tức này"><i class="fa fa-copy"></i>Copy</asp:LinkButton>
                                                            <asp:LinkButton class="btn btn-success btn-xs" ID="btnEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"newid")%>' CommandName="Edit" ToolTip="Sửa"><i class="fa fa-pencil-square-o"></i>Sửa</asp:LinkButton>
                                                            <asp:LinkButton class="btn btn-danger btn-xs" ID="btnDel" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"newid")%>' CommandName="Del" ToolTip="Xóa" OnClientClick="javascript:return confirm('Bạn có muốn xóa?');"><i class="fa fa-trash-o"></i>Xóa</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row dataTables_wrapper">
                            <div class="col-sm-5">
                                <div id="data-table_info" class="dataTables_info" role="status" aria-live="polite">
                                    <asp:Literal ID="ltrStatistic" runat="server"></asp:Literal>
                                </div>
                            </div>

                            <div class="col-sm-7">
                                <div id="data-table_paginate" class="dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <li id="data-table_previous" class="paginate_button previous disabled">
                                            <asp:LinkButton ID="btnPrevious" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Trước</asp:LinkButton>
                                        </li>
                                        <asp:Repeater ID="rptNumberPage" runat="server" OnItemCommand="rptNumberPage_ItemCommand" OnItemDataBound="rptNumberPage_ItemDataBound">
                                            <ItemTemplate>
                                                <asp:Literal ID="ltrLiPage" runat="server"></asp:Literal>
                                                <asp:LinkButton ID="btn" runat="server" CommandArgument='<%# Eval("PageIndex") %>' CommandName="page" Text='<%# Eval("PageText") %> '></asp:LinkButton></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <li id="data-table_next" class="paginate_button next">
                                            <asp:LinkButton ID="btnNext" runat="server" OnClick="btnPage_Click" CausesValidation="false" rel="nofollow">Sau</asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAddForder" runat="server" Visible="false">
        <h1 class="page-header">Thêm/sửa bài viết</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1">
                    <div class="panel-heading p-0">
                        <div class="panel-heading-btn m-r-10 m-t-10">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        </div>
                        <div class="tab-overflow">
                            <ul class="nav nav-tabs nav-tabs-inverse">
                                <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-inverse"><i class="fa fa-arrow-left"></i></a></li>
                                <li class="active"><a href="#info-tab" data-toggle="tab">Thông tin chung</a></li>
                                <li class=""><a href="#seo-tab" data-toggle="tab">Cấu hình SEO</a></li>
                                <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-inverse"><i class="fa fa-arrow-right"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="tab-content panel-body panel-form">
                        <asp:Panel ID="pnlErr2" runat="server" Visible="false">
                            <div class="alert alert-danger fade in" style="border-radius: 0px;">
                                <button class="close" data-dismiss="alert" type="button">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <asp:Literal ID="ltrErr2" runat="server"></asp:Literal>
                            </div>
                        </asp:Panel>
                        <div class="tab-pane fade active in" id="info-tab">
                            <div class="form-horizontal form-bordered">

                                <div class="form-group">
                                    <label class="control-label col-md-2">Nhóm bài viết *:</label>
                                    <div class="col-md-7">
                                        <asp:DropDownList ID="ddlForder" runat="server" class="form-control"></asp:DropDownList>
                                        <asp:HiddenField ID="hidID" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2">Tiêu đề *:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtTieudetin" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Thẻ alt:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtalt" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Nội dung tóm tắt:</label>
                                    <div class="col-md-7">
                                        <%--<asp:TextBox ID="txtTomtat" runat="server" class="form-control" TextMode="MultiLine" Height="200px"></asp:TextBox>--%>
                                        <CKEditor:CKEditorControl ID="txtTomtat" runat="server"></CKEditor:CKEditorControl>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Nội dung chi tiết:</label>
                                    <div class="col-md-7">
                                        <CKEditor:CKEditorControl ID="FckChitiet" runat="server"></CKEditor:CKEditorControl>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Ảnh đại diện:</label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <asp:TextBox ID="txtImage" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                <div class="input-group-btn">
                                                    <button onclick="BrowseServer('<% =txtImage.ClientID %>','News');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">File đính kèm:</label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                                <asp:TextBox ID="txtFile" runat="server" class="form-control" Style="width: 82%;"></asp:TextBox>
                                                <div class="input-group-btn">
                                                    <button onclick="BrowseServer('<% =txtImage.ClientID %>','Files');" class="btn btn-success" type="button" style="margin-left: -14px;">Browse Server</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Ngày đăng:</label>
                                    <div class="col-md-7">
                                        <div class="input-group date datepicker-autoClose">
                                            <asp:TextBox ID="txtNewsDate" runat="server" class="form-control datepicker-autoClose"></asp:TextBox>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>

                                <asp:Panel ID="pavi" runat="server" Visible="false">
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Thuộc sự kiện:</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="drlSukien" runat="server" class="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Hiển thị:</label>
                                    <div class="col-md-7">
                                        <asp:CheckBox ID="chkChuyenDe" runat="server" Checked="true" />Tin chuyên đề&nbsp;&nbsp;
                                        <asp:CheckBox ID="chkSukien" runat="server" />Tin sự kiện&nbsp;&nbsp; 
                                        <asp:CheckBox ID="chkUutien" runat="server" />Tin nổi bật&nbsp;&nbsp; 
                                        <asp:CheckBox ID="chkIndex" runat="server" />Giới thiệu trang chủ&nbsp;&nbsp;
                                        <asp:CheckBox ID="chkActive" runat="server" Checked="true" />Kích hoạt&nbsp;&nbsp;
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="tab-pane fade" id="seo-tab">
                            <div class="form-horizontal form-bordered">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Meta title:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtTieudetitle" runat="server" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Meta description:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtDesscription" runat="server" TextMode="MultiLine" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Meta keyword:</label>
                                    <div class="col-md-7">
                                        <asp:TextBox ID="txtKeyword" runat="server" TextMode="MultiLine" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-2">Tags sản phẩm:</label>
                                    <div class="col-md-7">
                                        <a onclick="javascript:ShowBlogNewOther('divRelate')" style="font-size: 20px; cursor: pointer;"><i class="fa ion-ios-pricetags"></i></a>
                                    </div>
                                </div>

                                <div id="divRelate" style="display: none;">
                                    <asp:UpdatePanel ID="udpTags" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="control-label col-md-2"></label>
                                                <div class="col-md-4">
                                                    <asp:ListBox ID="lstNew" runat="server" Rows="10" class="form-control"></asp:ListBox>
                                                </div>
                                                <div class="col-md-1">
                                                    <asp:Button class="btn btn-primary" ID="btnActive" runat="server" Text=">>" OnClick="btnActive_Click" />
                                                    <asp:Button class="btn btn-primary" ID="btnUnactive" runat="server" Text="<<" OnClick="btnUnactive_Click" />
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:ListBox ID="lstB" runat="server" Visible="false" class="form-control"></asp:ListBox>

                                                    <asp:ListBox ID="lstNewRelate" runat="server" Rows="10" class="form-control"></asp:ListBox>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>


                            </div>
                        </div>

                        <div class="form-horizontal form-bordered">
                            <div class="form-group" style="border-top: 1px solid #eee;">
                                <label class="control-label col-md-2"></label>
                                <div class="col-md-7">
                                    <asp:LinkButton ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" class="btn btn-primary">Cập nhật</asp:LinkButton>
                                    <asp:LinkButton ID="btnReset" runat="server" class="btn btn-danger" OnClick="btnReset_Click">Hủy</asp:LinkButton>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </asp:Panel>

</div>
