﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProjectRegister.ascx.cs" Inherits="MyWeb.views.pages.ucProjectRegister" %>
<div class="box-sub-prj-register prjRegisterForm">
            <div class="box-body-sub prjRegisterContent">
                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-md-4"></label>
                        <div class="col-md-8">
                            <span class="view-messenger">
                                <span id="thongbao"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-group prjRegisterName clearfix">
                        <label class="control-label col-md-4">
                            Họ tên(*):                           
                        </label>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtnamePrj" runat="server" class="form-control" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group prjRegisterEmail clearfix">
                        <label class="control-label col-md-4">
                            Email(*):        
                        </label>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtmailPrj" runat="server" class="form-control" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group prjRegisterPhone clearfix">
                        <label class="control-label col-md-4">
                            Số điện thoại(*):  
                        </label>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtDienthoaiPrj" runat="server" class="form-control" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group prjRegisterContent clearfix">
                        <label class="control-label col-md-4">Yêu cầu thêm (nếu có):</label>
                        <div class="col-md-8">
                            <asp:TextBox ID="txtnoidungPrj" runat="server" class="form-control" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group prjRegisterProject clearfix">
                        <label class="control-label col-md-4">Dự án:</label>
                        <div class="col-md-8">
                            <asp:DropDownList ID="ddlProject" runat="server" class="form-control" ClientIDMode="Static"></asp:DropDownList>
                            <asp:HiddenField ID="hidID" runat="server" />
                        </div>
                    </div>
                    <div class="form-group prjRegisterBtn">
                        <label class="control-label col-md-4"></label>
                        <div class="col-md-8">
                            <input type="button" class="btn btn-primary btn-md" onclick="SendData()" value="Gửi thông tin" id="btSend" />
                        </div>
                    </div>
                </div>
            </div>
</div>
<script>
    function MyFunction() {
        $("#myModal").modal("hide");
    }
    var r = {
        'special': /[\W]/g,
        'quotes': /[^0-9^]/g,
        'notnumbers': /[^a-zA]/g
    }
    function valid(o, w) {
        o.value = o.value.replace(r[w], '');
    }

    $('#txtThoiGian').datepicker({
        todayBtn: "linked",
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        startDate: '-3d',
    });
    function SendData() {
        $("#btSend").attr("disabled", "disabled");
        var hoten = $("#txtnamePrj").val();
        var email = $("#txtmailPrj").val();
        var phone = $("#txtDienthoaiPrj").val();
        var content = $("#txtnoidungPrj").val();
        var duan = $("#ddlProject").val();

        $.ajax({
            async: false,
            type: "POST",
            url: "webService.asmx/SendRegister",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ hoTen: hoten, email: email, phone: phone, content: content, duan: duan }),
            success: function (response) {
                console.log(response.d);
                if (response.d == true) {
                    $('#txtnamePrj').val("");
                    $('#txtmailPrj').val("");
                    $('#txtDienthoaiPrj').val("");
                    $('#txtnoidungPrj').val("");
                    $('#ddlProject').val("0");
                    $("#btSend").removeAttr("disabled");
                    $("span#thongbao").html("Bạn đã gửi thông tin thành công !!");
                } else {
                    $("#btSend").removeAttr("disabled");
                    $("span#thongbao").html("Lỗi không gửi được liên hệ !!");
                }
            },
            failure: function (response) {
                $("#btSend").removeAttr("disabled");
                $("span#thongbao").html("Lỗi không gửi được liên hệ !!");
            }

        });
    }
</script>
