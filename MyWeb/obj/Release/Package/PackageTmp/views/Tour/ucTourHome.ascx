﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucTourHome.ascx.cs" Inherits="MyWeb.views.Tour.ucTourHome" %>
<div id="listTourHome" class="listTour_all">
    <div class="listTour_label">
        <span>Tour trang chủ</span>
    </div>
    <div class="listTour_list">
        <asp:Literal runat="server" ID="ltrListItem"></asp:Literal>
    </div>
</div>
