﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucTourHot.ascx.cs" Inherits="MyWeb.views.Tour.ucTourHot" %>
<div id="listTourHot" class="listTour_all">
    <div class="listTour_label">
        <span>Tour nổi bật</span>
    </div>
    <div class="listTour_list">
        <asp:Literal runat="server" ID="ltrListItem"></asp:Literal>
    </div>
</div>
