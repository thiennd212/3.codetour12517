﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucPageDetail.ascx.cs" Inherits="MyWeb.views.pages.ucPageDetail" %>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f7e9d12301308fa" async="async"></script>
<div class="box-page-sub">
    <div class="cate-header sub-top">
        <div class="txt-name-sub">
            <asp:Literal ID="ltrName" runat="server"></asp:Literal>
        </div>        
    </div>
    <div class="box-body">
        <asp:Literal ID="ltrModules" runat="server"></asp:Literal>
        <asp:Literal runat="server" ID="ltrAdv"></asp:Literal>
        <div class="div-addthis-news">
            <div class="addthis_native_toolbox"></div>
        </div>
    </div>
    <%if (GlobalClass.commentFPage.Contains("1"))
      {%>
    <div class="clearfix">
        <div class="shares-box-comment">
            <div class="fb-comments" data-href="<%=ur %>" data-colorscheme="<%=cf %>" data-width="<%=wf %>" data-numposts="<%=nf %>"></div>
        </div>
    </div>
    <%} %>
</div>
