﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProMotion.ascx.cs" Inherits="MyWeb.views.products.ucProMotion" %>
<%if (showAdv)
  {%>
<div class="box-product-new box-product-motion">
    <div class="header">
        <%=MyWeb.Global.GetLangKey("pro_sl_km") %>
    </div>
    <ul class="body-pro">
        <asp:Literal ID="ltrProduct" runat="server"></asp:Literal>
    </ul>
</div>
<%} %>