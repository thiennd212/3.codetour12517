﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucBoxface.ascx.cs" Inherits="MyWeb.views.control.ucBoxface" %>
<div class="box-face">
    <div id="div-iframe"></div>
    <script type="text/javascript">
        function createIframe() {
            var i = document.createElement("iframe");
            i.src = "https://www.facebook.com/plugins/likebox.php?href=<%=u %>&amp;height=<%=h %>&amp;show_faces=true&amp;colorscheme=<%=c %>&amp;stream=false&amp;show_border=true&amp;header=true";
        i.scrolling = "auto";
        i.frameborder = "0";
        i.width = "100%";
        i.height = "277px";
        document.getElementById("div-iframe").appendChild(i);
    };
    if (window.addEventListener)
        window.addEventListener("load", createIframe, false);
    else if (window.attachEvent)
        window.attachEvent("onload", createIframe);
    else window.onload = createIframe;

    </script>
</div>
