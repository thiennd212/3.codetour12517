﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucMenuLeft2 : System.Web.UI.UserControl
    {
        string lang = "vi";
        string currLevel = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                currLevel = getCurrentLevel();
                BindData();
            }
        }

        private string getCurrentLevel()
        {
            tbPage curPage = null;
            string culevel = "";
            string curLink = "";
            var curURL = Request.RawUrl;
            curLink = Request["hp"] != null ? Request["hp"] : curURL.Substring(curURL.LastIndexOf("/") + 1);
            curLink = Request["hp"] != null ? curLink + ".html" : curLink;
            if (Request["hp"] != null && Request["hp"].ToString() != "")
            {
                string rqq = Request["hp"].ToString();
                curPage = db.tbPages.FirstOrDefault(s => s.pagLink == curLink && s.pagType < 100);
            }
            var homePage = db.tbPages.FirstOrDefault(s => s.pagLink == "/" && s.pagTagName.Contains("trang-chu"));
            if (homePage != null)
                culevel = homePage.paglevel;
            //viet them
            try
            {
                string pagtag = Request["hp"] != null ? Request["hp"] : curURL.Substring(curURL.LastIndexOf("/") + 1);
                var pagtytin = db.tbPages.Where(u => u.pagTagName == pagtag).FirstOrDefault();
                if (pagtytin.pagType == 800)
                {
                    var tbnew = db.tbNews.FirstOrDefault(u => u.newTagName == pagtag);
                    var data = db.tbPages.Where(u => u.pagId == tbnew.grnID).FirstOrDefault();
                    var xx = db.tbPages.Where(u => u.paglevel == data.paglevel.Substring(0, 5)).FirstOrDefault();
                    string chuoi = xx.pagTagName + ".html";
                    var kq = db.tbPages.Where(u => u.pagLink == chuoi).FirstOrDefault();
                    return kq.paglevel.Substring(0, 5);
                }
            }
            catch (Exception)
            {

            }
            if (curPage != null)
            {
                return curPage.paglevel;
            }
            else
            {
                return culevel;
            }
        }

        private void BindData()
        {
            string _strMenu = "";
            List<tbPageDATA> _objList = tbPageDB.tbPage_GetBypagPosition("3", lang);

            if (_objList.Count > 0)
            {
                int ilevel = 0, iblevel = 0, ilength = 5, istartlevel = 0, k = 0, n = 1;
                string _strLevel = "";
                for (int j = 0; j < _objList.Count; j++)
                {
                    string _strLiClass = "";
                    _strLevel = _objList[j].paglevel;
                    ilevel = (_strLevel.Length / ilength) - istartlevel;
                    if (ilevel > iblevel)
                    {
                        _strMenu += ilevel == 1 ? string.Format("<ul id=\"{0}\" class=\"menu-left-view\">", "menu_left") : "<ul class=\"sub-menu\">";
                    }
                    if (ilevel < iblevel)
                    {
                        for (k = 1; k <= (iblevel - ilevel); k++)
                        {
                            _strMenu += "</ul>";
                            if (iblevel > 1) { _strMenu += "</li>"; }
                        }
                        iblevel = ilevel;
                    }
                    string _strName = _objList[j].pagName;

                    

                    string lastClass = j == _objList.Count - 1 ? "last" : "";
                    string iconClass = tbPageDB.tbPage_ExitstByLevel(_strLevel).Count > 0 ? ilevel == 1 ? "itop" : "icon" : "";
                    if (iconClass.Length > 0)
                        _strLiClass += " " + iconClass;
                    if (lastClass.Length > 0)
                        _strLiClass += " " + lastClass;
                    if (_strLiClass.Length > 0)
                        _strLiClass = string.Format(" class=\"{0} list-item\"", _strLiClass.Trim());

                    string _strUrlOut = "";
                    string _strLink = "";

                    if (_objList[j].pagType == "3")
                    {
                        _strUrlOut = MyWeb.Common.StringClass.GetContent(_objList[j].pagLink, 7);
                        if (!(_strUrlOut == "http://..."))
                        {
                            _strUrlOut = "http://";
                        }
                        else
                        {
                            _strUrlOut = "";
                        }
                    }
                    _strLink = _objList[j].pagType == "3" ? _objList[j].pagLink : _objList[j].pagLink;

                    _strMenu += "<li" + _strLiClass + "><span class=\"span-menu\"></span><a class=\"link-menu-left\" href=\"" + _strLink + "\" target=\"" + _objList[j].pagTarget + "\">" + _objList[j].pagName + "</a>";

                    if (tbPageDB.tbPage_ExitstByLevel(_strLevel).Count == 0)
                    {
                        _strMenu += "</li>";
                    }

                    iblevel = ilevel;
                    if (n == _objList.Count)
                    {
                        k = 0;
                        for (k = iblevel - 1; k == 1; k--)
                        {
                            _strMenu += "</ul>";
                            if (iblevel > 1) { _strMenu += "</ul></li>"; }
                        }
                    }
                    n++;
                }
                _strMenu += "</ul>";
            }
            _objList.Clear();
            _objList = null;
            ltrMenuLeft.Text = _strMenu;

        }
    }
}