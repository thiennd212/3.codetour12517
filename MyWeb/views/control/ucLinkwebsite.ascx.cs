﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.control
{
    public partial class ucLinkwebsite : System.Web.UI.UserControl
    {
        public bool showAdv = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            string str = "";
            str += "<select id=\"slLink\" class=\"slLink\" name=\"slLink\" onchange=\"changeLink(this.value)\">";
            str += "<option value=\"\">-- " + MyWeb.Global.GetLangKey("link_website") + " --</option>";
            List<tbAdvertiseDATA> obj = tbAdvertiseDB.tbAdvertise_GetByPosition_Vietime("11");
            if (obj.Count() > 0)
            {
                for (int i = 0; i < obj.Count; i++)
                {
                    str += "<option value=\"" + obj[i].advLink + "\">" + obj[i].advName + "</option>";
                }
            }
            else
            {
                showAdv = false;
            }

            str += "</select>";
            ltrLinkWeb.Text = str;
        }
    }
}