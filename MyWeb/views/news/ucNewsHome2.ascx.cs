﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.news
{
    public partial class ucNewsHome2 : System.Web.UI.UserControl
    {
        string lang = "vi";
        private string strNumberView = GlobalClass.viewNews2;
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            int count = 1;
            List<tbPageDATA> groupNews = tbPageDB.tbPage_GetByAll(lang);
            groupNews = groupNews.Where(s => s.pagType == "200" && s.check1 == "1" && s.pagActive == "1" && s.pagLang.Trim() == lang).ToList();
            string _str = "";
            foreach (tbPageDATA grn in groupNews)
            {
                string grnLevel = grn.paglevel;
                var data = db.tbPages.Where(u => u.paglevel.Substring(0, grnLevel.Length) == grnLevel).Select(u => u.pagId).ToArray();
                IEnumerable<tbNew> news = db.tbNews.Where(s => s.newLang.Trim() == lang && s.newActive == 0 && data.Contains(s.grnID.Value));
                news = news.OrderByDescending(s => s.newDate).ThenByDescending(s => s.newid).ToList();
                news = news.Skip(0).Take(int.Parse(strNumberView));
                if (news.Count() > 0)
                {
                    _str += "<div class=\"box-news-list\" id='listNewType" + count + "'>";
                    _str += "<div class=\"header\"><a href=\"/" + grn.pagTagName + ".html\" >" + grn.pagName + "</a></div>";
                    _str += "<div class=\"body-news clearfix\">";
                    
                    int i = 0;
                    if ( count == 1 || count == 2)
                    {                       
                        foreach (tbNew n in news)
                        {
                            string alt = n.newAlt != "" && n.newAlt != null ? n.newAlt : n.newName;
                            string link = "/" + n.newTagName + ".html";
                            string image = n.newImage != null && n.newImage.Trim() != "" ? string.Format("<a href=\"{0}\" title=\"{1}\"><img src=\"{2}\" alt=\"{3}\" title=\"{1}\" /></a>", link, n.newName, n.newImage, alt) : "";
                            if (i == 0)
                                _str += String.Format("<div class=\"item-list clearfix\">{2}<a href=\"{0}\" title=\"{1}\">{1}</a><div class=\"clearfix\">{3}</div><div class=\"news-more\"><a href=\"{0}\">" + MyWeb.Global.GetLangKey("news_more") + "</a></div></div>", link, n.newName, image, n.newContent.ToString());
                            else
                            {
                                if (i == 1)
                                    _str += "<div class=\"item-list clearfix\"><ul>";
                                _str += String.Format("<li><a href=\"" + link + "\" title=\"" + n.newName + "\">" + n.newName + "</a> </li>");
                                if (i == news.Count() - 1)
                                    _str += "</ul></div>";
                            }                            
                        }
                    }
                    else
                    {
                        foreach (tbNew n in news)
                        {
                            string alt = n.newAlt != "" && n.newAlt != null ? n.newAlt : n.newName;
                            string link = "/" + n.newTagName + ".html";
                            string image = n.newImage != null && n.newImage.Trim() != "" ? string.Format("<a href=\"{0}\" title=\"{1}\"><img src=\"{2}\" alt=\"{3}\" title=\"{1}\"/></a>", link, n.newName, n.newImage, alt) : "";
                            _str += String.Format("<div class=\"item-list clearfix\">{2}<a href=\"{0}\" title=\"{1}\">{1}</a><div class=\"view-container\">{3}</div><div class=\"news-more\"><a href=\"{0}\">" + MyWeb.Global.GetLangKey("news_more") + "</a></div></div>", link, n.newName, image, n.newContent.ToString());                            
                        }
                    }
                    i++;
                    _str += "</div></div>";
                }
                count++;
            }
            ltrNewsHome.Text = _str;
        }
    }
}