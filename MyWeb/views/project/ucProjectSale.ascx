﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProjectSale.ascx.cs" Inherits="MyWeb.views.project.ucProjectSale" %>
<div id="listProjectSale" class="listProject_all">
    <div class="listProject_label">
        <span>DỰ ÁN SALE</span>
    </div>
    <div class="listProject_list">
        <asp:Literal runat="server" ID="ltrListItem"></asp:Literal>
    </div>
</div>
<script type="text/javascript">
    function ShareFB(url) {
    FB.ui(
            {
                method: 'share',
                href: url,
            },
            // callback
            function (response) {
                if (response && !response.error_message) {
                    console.log('Posting completed.');
                } else {
                    console.log('Error while posting.');
                }
            }
        );
};
</script>
