﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.project
{
    public partial class detailProject : System.Web.UI.UserControl
    {
        public string strUrlFace = "";
        public string wf = "770";
        public string nf = "5";
        public string cf = "light";
        int currentPage;
        dataAccessDataContext db = new dataAccessDataContext();
        protected string lang = "vi";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (GlobalClass.widthfkComment != null) { wf = GlobalClass.widthfkComment; }
            if (GlobalClass.numfkComment != null) { nf = GlobalClass.numfkComment; }
            if (GlobalClass.colorfkComment != null) { cf = GlobalClass.colorfkComment; }
            lang = MyWeb.Global.GetLang();
            currentPage = Request.QueryString["page"] != null ? int.Parse(Request.QueryString["page"]) : 1;
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void LoadData()
        {
            strUrlFace = Request.Url.ToString();
            if (Request["hp"] != null)
            {
                string tagName = Request["hp"].ToString();
                var item = db.tbProjects.FirstOrDefault(s => s.proTagName == tagName);

                if (item != null)
                {
                    string strInfo = "", strTab = "", strOther="";

                    hdProjId.Value = item.proId.ToString();
                    string imgUrl = (!string.IsNullOrEmpty(item.proImage))?item.proImage.Split(Convert.ToChar(","))[0]:"";
                    strInfo += "<div class=\"infoTop\">";
                    strInfo += "<div class=\"infoTopHead\">DỰ ÁN</div>";
                    strInfo += "<h1 class=\"projName\">" + item.proName + "</h1>";
                    strInfo += "<img src='" + imgUrl + "' alt='" + item.proName + "' />";
                    strInfo += "</div>";
                    ltrInfo.Text = strInfo;

                    BindTabs(item.proId);

                    // Other
                    var listOther =
                        db.tbProjects.Where(
                            s =>
                                s.proLang == lang && s.proActive == 1 && s.catId == item.catId &&
                                s.proId != item.proId).OrderByDescending(s => s.proId).Take(3).ToList();
                    strOther += "<div class=\"titleOtherProj\">Dự án liên quan</div>";

                    foreach (var itemx in listOther)
                    {
                        string imgUrlx = (!string.IsNullOrEmpty(itemx.proImage)) ? itemx.proImage.Split(Convert.ToChar(","))[0] : "";
                        strOther += "<div class='projItems'>";
                        strOther += "  <div class='projItems_info1'>";
                        strOther += "      <a class='projItems_img' href='/" + itemx.proTagName + ".html' title='" + itemx.proName + "'>";
                        strOther += "          <img src='" + imgUrlx + "' alt='" + itemx.proName + "' />";
                        strOther += "          <span class='projItems_priceOrifin'> $" + common.FormatNumber(item.proPrice) + "/m2</span>";
                        strOther += "      </a>";
                        strOther += "      <a class='projItems_link' href='/" + itemx.proTagName + ".html' title='" + itemx.proName + "'>" + itemx.proName + "</a>";
                        strOther += "      <div class='projItems_des'>" + itemx.proWarranty + "</div>";
                        strOther += "  </div>";
                        strOther += "</div>";
                    }

                    ltrOther.Text = strOther;
                }
                else
                {
                    Response.Redirect("/error404.html");
                }
            }
            else
            {
                Response.Redirect("/error404.html");
            }
        }

        void BindTabs(int prjId)
        {
            string _strTabsName = "";
            string _strTabs = "";
            List<tbProjectTag> lst = db.tbProjectTags.Where(s => s.proId == prjId).OrderBy(s => s.Ord).ToList();
            if (lst.Count > 0)
            {
                _strTabsName += "<ul class=\"tabs-pro\">";
                for (int i = 0; i < lst.Count; i++)
                {
                    if (i == 0)
                    {
                        _strTabsName += "<li class=\"active\" rel=\"tab" + i + "\">" + lst[i].TagName + "</li>";
                    }
                    else
                    {
                        _strTabsName += "<li rel=\"tab" + i + "\">" + lst[i].TagName + "</li>";
                    }
                    _strTabs += "<div id=\"tab" + i + "\" class=\"tab_content\">" + lst[i].TagValue + "</div>";

                }
                _strTabsName += "</ul>";
            }
            ltrTabs.Text = _strTabsName + _strTabs;
        }

        protected string getCommentPaging(int cur, int size, int total)
        {
            string strResult = "";
            var numberOfPage = total % size == 0 ? total / size : (total / size) + 1;
            strResult += "<ul class='paging'>";
            var curUrl = Request.RawUrl;
            curUrl = curUrl.IndexOf("?") > 0 ? curUrl.Substring(0, curUrl.IndexOf("?")) : curUrl;
            for (int i = 1; i <= numberOfPage; i++)
            {
                strResult += i == cur ?
                    "<li class='active'>" + i + "</li>" :
                    "<li class='item'><a href='" + curUrl + "?page=" + i + "' title='Trang " + i + "'>" + i + "</a></li>";
            }
            strResult += "</ul>";
            return strResult;
        }

        protected string GetName(int plId)
        {
            var item = db.tbPages.FirstOrDefault(s => s.pagId == plId);

            return item != null ? item.pagName : "";
        }

        protected string GetListPlaceName(int tourId)
        {
            var listPl = db.TourPlaces.Where(s => s.IdTour == tourId).Select(s => s.IdPlace).ToList();
            var listPage = db.tbPages.Where(s => listPl.Contains(s.pagId)).ToList();
            string strInfoRight = "";

            if (listPage.Any())
            {

                foreach (var item in listPage)
                {
                    strInfoRight += strInfoRight != "" ? "</br><span class='itemTours_placeItem'>" + item.pagName + "</span>" : "<span class='itemTours_placeItem'>" + item.pagName + "</span>";
                }
            }

            return strInfoRight;
        }
    }
}