﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProHot.ascx.cs" Inherits="MyWeb.views.products.ucProHot" %>
<%if (showAdv)
  {%>
<div class="box-product-new box-product-hot">
    <div class="header">
        <%=MyWeb.Global.GetLangKey("pro_sl_nb") %>
    </div>
    <ul class="body-pro">
        <asp:Literal ID="ltrProduct" runat="server"></asp:Literal>
    </ul>
</div>
<%} %>