﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucProContact : System.Web.UI.UserControl
    {
        string lang = "vi"; 
        dataAccessDataContext db= new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();
            BindProCatName();
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
        }

        protected void BindProCatName() 
        {
            List<tbPage> list = db.tbPages.Where(x => x.pagLang == lang
                && x.pagActive == 1 && x.paglevel.Length == 5
                && x.pagType + "" == pageType.GP && x.check3.ToString() == "1").OrderByDescending(x => x.pagName).ToList();
            
            chkListDongXe.Items.Clear();            
            for (int i = 0; i < list.Count; i++)
            {
                chkListDongXe.Items.Add(new ListItem(list[i].pagName, list[i].pagName));
            }
            list.Clear();
            list = null;
        }
    }
}