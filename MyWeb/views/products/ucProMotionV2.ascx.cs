﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyWeb.views.products
{
    public partial class ucProMotionV2 : System.Web.UI.UserControl
    {
        string lang = "vi";
        public bool showAdv = false;
        string viewBy = GlobalClass.viewProducts9;
        private string strNumberView = "";
        dataAccessDataContext db = new dataAccessDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            lang = MyWeb.Global.GetLang();

            if (!IsPostBack)
            {
                BindData();
            }
        }
        private void BindData()
        {
            string _str = "";
            if (GlobalClass.viewProducts12 != "") { strNumberView = GlobalClass.viewProducts12; }
            IEnumerable<tbProduct> objProduct = db.tbProducts.Where(s => s.proActive == 1 && s.proKM == 1 && s.proLang.Trim() == lang).OrderByDescending(s => s.proDate).ThenByDescending(s => s.proId);
            objProduct = objProduct.Skip(0).Take(int.Parse(strNumberView));
            if (objProduct.Count() > 0)
            {
                showAdv = true;
                //int i = 0;
                //_str += "<ul class=\"product-list-promo\">";
                //_str += common.LoadProductList(objProduct, viewBy);
                //_str += "</ul>";                
                //ltrProduct.Text = _str;
                ltrProduct.Text = common.LoadProductListV2(objProduct);
            }
        }
    }
}